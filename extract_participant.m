%% extract_participant
% 
% Extracts the runs of a specific participant.
%
%% Synopsis
%
%   [] = extract_participant(partID)
%  
%
% *Parameters*
%
% * *|partID|* - the identifier of the participant to extract.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = extract_participant(partID)

    % check that we have the correct number of input arguments. 
    narginchk(1, 1);
    
    % setup common parameters
    common_parameters;
        
    % check that detID is a non-empty string
    validateattributes(partID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'detID');

     if iscell(partID)
        % check that detID is a cell array of strings with one element
        assert(iscellstr(partID) && numel(partID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected detID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    partID = char(strtrim(partID));
    partID = partID(:).';
    
    % check that partID assumes a valid value
    validatestring(partID, ...
        EXPERIMENT.participant.list, '', 'partID');    
                  
    % start of overall computations
    startComputation = tic;

    
    % copy configuration to local aliases
    trackID  = EXPERIMENT.participant.(partID).track;
    partTrackID = partID;
    
    
    fprintf('\n\n######## Extract participant run %s of track %s (%s) ########\n\n', ...
        partID, trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - participant: %s - %s \n', partID, EXPERIMENT.participant.(partID).org);
    fprintf('    * runs: %d \n', EXPERIMENT.participant.(partID).runs.number);
    fprintf('    * track: %s \n\n', trackID);
    
    
    
    fprintf('+ Processing pool\n');
    
    poolID = EXPERIMENT.pattern.identifier.pool(EXPERIMENT.track.(trackID).corpus, trackID);
    fprintf('  - source pool %s\n', poolID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    poolID = EXPERIMENT.pattern.identifier.pool(EXPERIMENT.track.(trackID).corpus, partTrackID);
    
    pool.Properties.UserData.identifier = poolID;
    
    sersave2(EXPERIMENT.pattern.file.dataset.corpus(partTrackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    fprintf('  - target pool %s\n', poolID);
    
    
    fprintf('+ Loading runs\n');
    
    runsetID = EXPERIMENT.pattern.identifier.run(EXPERIMENT.track.(trackID).corpus, trackID);
    fprintf('  - %s\n', runsetID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, runsetID), ...
        'WorkspaceVarNames', {'runSet'}, ...
        'FileVarNames', {runsetID});
    
    fprintf('+ Extracting runs of participant %s\n', partID);
    fprintf('  ');
    
    runsetID = EXPERIMENT.pattern.identifier.run(EXPERIMENT.track.(trackID).corpus, partTrackID);
    
    % select the participant runs and create a runSet containing them
    runSet = runSet(:, EXPERIMENT.participant.(partID).runs.list);
    runSet.Properties.UserData.runs = [EXPERIMENT.participant.(partID).runs.list];
    runSet.Properties.UserData.identifier = runsetID;
         
    sersave2(EXPERIMENT.pattern.file.dataset.corpus(partTrackID, runsetID), ...
        'WorkspaceVarNames', {'runSet'}, ...
        'FileVarNames', {runsetID});
    
    
    fprintf('\n\n######## Total elapsed time for extracting runs of participant %s of track %s (%s) ########\n\n', ...
        partID, trackID, elapsedToHourMinutesSeconds(toc(startComputation)));    

end


