%% compute_mdf1to2_analysis
% 
% Computes the Fake ANOVA on the whole corpus.

%% Synopsis
%
%   [] = compute_mdf1to2_analysis(trackID, tsID, startMeasure, endMeasure, threads)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tsID|* - the identifier of the topic sampling process to apply.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
% * *|threads|* - the maximum number of threads to be used. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = compute_mdf1to2_analysis(trackID, tsID, startMeasure, endMeasure, threads)

    persistent TAG MDFH0 MDFH1;
    
    if isempty(TAG)
        MDFH0 = 'mdf12h0';
        MDFH1 = 'mdf12h1';
        TAG = {MDFH1};      
    end

    % check the number of input parameters
    narginchk(2, 5);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
                 
    % check that tsID is a non-empty string
    validateattributes(tsID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'smplID');

    if iscell(tsID)
        % check that tsID is a cell array of strings with one element
        assert(iscellstr(tsID) && numel(tsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tsID = char(strtrim(tsID));
    tsID = tsID(:).';
    
    % check that smplID assumes a valid value
    validatestring(tsID, ...
        EXPERIMENT.analysis.topicSampling.list, '', 'tsID');  
                    
    if nargin >= 3
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
        
    if nargin == 5
        % the number of threads must be at maximum equal to the number of
        % physical cores
        validateattributes(threads, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', feature('numcores') }, '', 'threads');
        
        maxNumCompThreads(threads);
    else
        threads = maxNumCompThreads('automatic');        
    end
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Performing Fake ANOVA analyses on whole corpus on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * topic sampling: %s\n', tsID);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - threads: %d\n\n', threads);
    
    
    fprintf('\n+ Loading topic samples \n');
    
    tpc1ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(1, tsID, trackID);
    tpc2ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(2, tsID, trackID);
    
    serload2(EXPERIMENT.pattern.file.dataset.topic(trackID, EXPERIMENT.pattern.identifier.topicSampling.tsetID(tsID, trackID)), ...
        'WorkspaceVarNames', {'tpc1', 'tpc2'}, ...
        'FileVarNames', {tpc1ID, tpc2ID});
    
    % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
        
        % load the whole corpus measure
        measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.track.(trackID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % the mean of the measure on the whole corpus
        mm = mean(measure{:, :});
        
        % order systems by descending mean of the measure on the whole corpus
        [~, idx] = sort(mm, 'descend');
        
        % reorder systems by descending mean of the measure on the
        % whole corpus
        measure = measure(:, idx);
        
        % the number of topics and runs
        T.tpc1 = size(tpc1, 2);
        T.tpc2 = size(tpc2, 2);
        T.size = height(measure);
        R = width(measure);
        
        % total number of elements in the list
        N.tpc1 = T.tpc1 * R;
        N.tpc2 = T.tpc2 * R;
        
        
        % for each analysis type
        for md = 1:length(TAG)
            
            start = tic;
            fprintf('  - performing %s ANOVA analysis\n', TAG{md});
            
            % the ANOVA model applied to each topic set
            MD.tpc1 = TAG{md};
            MD.tpc2 = TAG{md};
            
            % preallocate data structures
            [tbl, soa, me, cmp] = md_data_structures(measure.Properties.VariableNames, R, T, MD);
            
            
            fprintf('    * topic set sample ');
            
            % perform analyses on each topic set sample
            for ts = 1:EXPERIMENT.analysis.topicSampling.samples
                
                fprintf('.');
                
                % extract the first topic set
                data.tpc1 = measure{tpc1(ts, :), :};
                data.tpc1 = data.tpc1(:);
                
                % labels for the ANOVA analysis
                subject.tpc1 = repmat(measure.Properties.RowNames(tpc1(ts, :)), R, 1);
                factorA.tpc1 = repmat(me.factorA.label, T.tpc1, 1);
                factorA.tpc1 = factorA.tpc1(:);
                
                % extract the second topic set
                data.tpc2 = measure{tpc2(ts, :), :};
                data.tpc2 = data.tpc2(:);
                
                % labels for the ANOVA analysis
                subject.tpc2 = repmat(measure.Properties.RowNames(tpc2(ts, :)), R, 1);
                factorA.tpc2 = repmat(me.factorA.label, T.tpc2, 1);
                factorA.tpc2 = factorA.tpc2(:);
                                
                % perform the ANOVA analysis
                [~, tbl.tpc1{ts}, sts.tpc1] = EXPERIMENT.analysis.(TAG{md}).compute(data.tpc1, factorA.tpc1);
                [~, tbl.tpc2{ts}, sts.tpc2] = EXPERIMENT.analysis.(TAG{md}).compute(data.tpc2, factorA.tpc2);
                
                % extract info from the ANOVA tables
                sts.df_factorA.tpc1 = tbl.tpc1{ts}{2,3};
                sts.ss_factorA.tpc1 = tbl.tpc1{ts}{2,2};
                sts.F_factorA.tpc1 = tbl.tpc1{ts}{2,6};
                
                sts.df_error.tpc1 = tbl.tpc1{ts}{3,3};
                sts.ss_error.tpc1 = tbl.tpc1{ts}{3,2};
                sts.ms_error.tpc1 = tbl.tpc1{ts}{3,5};
                
                sts.df_factorA.tpc2 = tbl.tpc2{ts}{2,3};
                sts.ss_factorA.tpc2 = tbl.tpc2{ts}{2,2};
                sts.F_factorA.tpc2 = tbl.tpc2{ts}{2,6};
                
                sts.df_error.tpc2 = tbl.tpc2{ts}{3,3};
                sts.ss_error.tpc2 = tbl.tpc2{ts}{3, 2};
                sts.ms_error.tpc2 = tbl.tpc2{ts}{3, 5};
                
                % indicate that power has not to be computed
                sts.computePower = false;
                
                % compute SoA, main effects, multiple comparisons, ...
                [soa, me] = md_factorA_analysis(TAG{md}, ts, sts, N, T, R, NaN, data, factorA, soa, me);
                
                
                % tweak the computations
                soa.omega2p.factorA.tpc1(ts) = NaN;
                soa.omega2p.factorA.tpc2(ts) = NaN;
                
                me.factorA.tukey.q.tpc1(ts) = NaN;
                me.factorA.tukey.q.tpc2(ts) = NaN;
                
                me.factorA.tukey.halfWidth.tpc1(ts) = NaN;
                me.factorA.tukey.halfWidth.tpc2(ts) = NaN;
                                
                switch TAG{md}
                    case MDFH0 % set everything to not significant
                        me.factorA.tukey.mc.tpc1{ts}(:, 6) = 1.0;
                        me.factorA.tukey.mc.tpc2{ts}(:, 6) = 1.0;
                    case MDFH1 % set everything to significant
                        me.factorA.tukey.mc.tpc1{ts}(:, 6) = 0.0;
                        me.factorA.tukey.mc.tpc2{ts}(:, 6) = 0.0;
                end
                                
                % compare the two topic sets on the above analyses
                [cmp] = md_factorA_compare_ts(ts, me, soa, cmp);
                   
            end  % for topic samples
            
            fprintf('\n');
            
            % compute summary statistics about the comparison between the
            % two topic sets;
            [cmp] = md_factorA_summary_ts(cmp);
            
            
            % save data
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            
            sersave2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                'WorkspaceVarNames', {'me', 'tbl', 'soa', 'cmp'}, ...
                'FileVarNames', {anovaMeID, anovaTableID, anovaSoAID, anovaCmpID});
            
            clear data me tbl sts soa cmp;
            
            fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
        end % for analysis
        
        clear mm idx measure T R  N MD
        
        
    end % for measure
                   
    fprintf('\n\n######## Total elapsed time for performing Fake ANOVA analyses on whole corpus on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

