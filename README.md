Matlab source code for running the experiments reported in the paper:

* Ferro, N., and Sanderson, M. (2022). How do you Test a Test? A Multifaceted Examination of Significance Tests. _Proc. 15th ACM International Conference on Web Searching and Data Mining (WSDM 2022)_.