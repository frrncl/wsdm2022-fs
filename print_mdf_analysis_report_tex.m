%% print_mdf_analysis_report_tex
% 
% Reports a summary about the Fake ANOVA analyses and saves them to a |.tex|
% file.
%
%% Synopsis
%
%   [] = print_mdf_analysis_report_tex(trackID, shardIDs, tsIDs, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|shardIDs|* - a cell array with the identifier of the shards to be used.
% * *|tsID|* - a cell array with the identifiers of the topic sampling process to apply.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2019-2020 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_mdf_analysis_report_tex(trackID, shardIDs, tsIDs, startMeasure, endMeasure)

    persistent MDF12H0 MDF12H1 MDF3H0 MDF3H1 SHARD_SAMPLE;

    if isempty(MDF12H0)
        MDF12H0 = 'mdf12h0';
        MDF12H1 = 'mdf12h1';
        MDF3H0 = 'mdf3h0';
        MDF3H1 = 'mdf3h1';
        SHARD_SAMPLE = 1;
    end

    % check the number of input parameters
    narginchk(3, 5);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    
    for s = 1:length(shardIDs)
        
        shardID = shardIDs{s};
        
        % check that shardID is a non-empty cell array
        validateattributes(shardID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');
        
        if iscell(shardID)
            % check that shardID is a cell array of strings with one element
            assert(iscellstr(shardID) && numel(shardID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        shardID = char(strtrim(shardID));
        shardID = shardID(:).';
        
        % check that shardID assumes a valid value
        validatestring(shardID, ...
            EXPERIMENT.shard.list, '', 'shardID');
        
        % check that the track and the shard rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.shard.(shardID).corpus), 'Track %s and shard %s do not rely on the same corpus', trackID, shardID);
        
    end % for splitIDs
    
    for s = 1:length(tsIDs)
        
        tsID = tsIDs{s};
        
        % check that tsID is a non-empty cell array
        validateattributes(tsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'tsID');
        
        if iscell(tsID)
            % check that tsID is a cell array of strings with one element
            assert(iscellstr(tsID) && numel(tsID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        tsID = char(strtrim(tsID));
        tsID = tsID(:).';
        
        % check that tsID assumes a valid value
        validatestring(tsID, ...
            EXPERIMENT.analysis.topicSampling.list, '', 'tsID');
        
    end % for tsIDs

    if nargin == 5
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;


    fprintf('\n\n######## Reporting summary Fake ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - track: %s\n', trackID);
    
    fprintf('  - shards:\n');
    for s = 1:length(shardIDs)
        
        shardID = shardIDs{s};        
        
        fprintf('    * shard: %s - %d shards\n', shardID, EXPERIMENT.shard.(shardID).shard);
    end
    
    fprintf('  - topic sampling:\n');
    for s = 1:length(tsIDs)
        
        tsID = tsIDs{s};        
        
        fprintf('    * topic sampling: %s - %s (%d%% - %d%%)\n', ...
            EXPERIMENT.analysis.topicSampling.(tsID).label,  ...
            EXPERIMENT.analysis.topicSampling.(tsID).description, ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc1 * 100), ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc2 * 100));
    end
    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

        
    fprintf('+ Printing the report\n');
    fprintf('  ');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.report.mdfanalysis(trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID, 'tex'), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage[sort&compress,colon]{natbib}\n');       
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{longtable}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a2paper,landscape]{geometry}\n\n');
    
    
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    
    fprintf(fid, '\\title{Report on Fake ANOVA Analyses on %s}\n\n', ...
        strrep(trackID, '_', '\_'));
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
    fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
    fprintf(fid, '\\item system pairs: %d \n', EXPERIMENT.track.(trackID).runs*(EXPERIMENT.track.(trackID).runs-1)/2);
    fprintf(fid, '\\end{itemize}\n');
            
    fprintf(fid, '\\item topic set sampling:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item the topic set sampling is repeated %d times;\n', EXPERIMENT.analysis.topicSampling.samples);
    for s = 1:length(tsIDs)
        
        tsID = tsIDs{s};        
        
        fprintf(fid, '\\item \\texttt{%s}: %s (%d\\%% - %d\\%%);\n', ...
            strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'),  ...
            EXPERIMENT.analysis.topicSampling.(tsID).description, ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc1 * 100), ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc2 * 100));
    end
    fprintf(fid, '\\end{itemize}\n');
    
    
    fprintf(fid, '\\item shards:\n');    
    fprintf(fid, '\\begin{itemize}\n');
    for s = 1:length(shardIDs)
        
        shardID = shardIDs{s};
                        
        fprintf(fid, '\\item \\texttt{%s}: %s;\n',  strrep(shardID, '_', '\_'),  EXPERIMENT.shard.(shardID).name);
    end
    fprintf(fid, '\\end{itemize}\n');

    
    fprintf(fid, '\\item ANOVA models:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MDF12H0, EXPERIMENT.analysis.(MDF12H0).name);
    fprintf(fid, '\\item %s: %s \n', MDF12H1, EXPERIMENT.analysis.(MDF12H1).name);
    fprintf(fid, '\\item %s: %s \n', MDF3H0, EXPERIMENT.analysis.(MDF3H0).name);
    fprintf(fid, '\\item %s: %s \n', MDF3H1, EXPERIMENT.analysis.(MDF3H1).name);
            
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item significance level $\\alpha = %4.3f$:\n', EXPERIMENT.analysis.alpha.threshold);
    
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n\n');
    
    fprintf(fid, '\\vspace*{2em}\n\n');
                     
        
    fprintf(fid, 'Each table reports:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item $\\mathbf{\\tau}$ \\textbf{avg.}: the correlation between the rankings of systems in the two topic sets, averaged over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item $\\mathbf{\\hat{\\omega}^2}$: \n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{TS1 avg.}: average of the effect sizes of the system factor in topic set 1, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{TS2 avg.}: average of the effect sizes of the system factor in topic set 2, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{p-value}: the p-value of an unpaired t-test between the effect sizes of the system factor over the %d samples of topic sets to tell whether they are significantly different in the two topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\end{itemize}\n');
    fprintf(fid, '\\item \\textbf{Tukey CI}:\n');    
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{TS1 avg.}: average of the Tukey confidence intervals of the system factor in topic set 1, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{TS2 avg.}: average of the Tukey confidence intervals of the system factor in topic set 2, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{p-value}: the p-value of an unpaired t-test between the Tukey confidence intervals of the system factor over the %d samples of topic sets to tell whether they are significantly different in the two topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\end{itemize}\n');
    fprintf(fid, '\\item \\textbf{System Pairs}: the outcomes of the (multiple) comparison of all the possible system pairs according to the Tukey HSD test;\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{Size}:\n'); 
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{TS1 avg.}: average of the size of the set of significantly different systems in topic set 1, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{TS2 avg.}: average of the size of the set of significantly different systems in topic set 2, over the %d samples of topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{p-value}: the p-value of an unpaired t-test between the sizes of the sets of significantly different systems over the %d samples of topic sets to tell whether they are significantly different in the two topic sets;\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\end{itemize}\n');    
    fprintf(fid, '\\item \\textbf{Jaccard}: the Jaccard coefficient $\\frac{|TS1 \\cap TS2|}{|TS1 \\cup TS2|}$ over the %d samples of topic sets; see \\cite{Jaccard1901}.\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{Overlap}: the Overlap coefficient $\\frac{|TS1 \\cap TS2|}{\\min(|TS1|, |TS1|)}$ over the %d samples of topic sets; see \\cite{Szymkiewicz1934}.\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{AA} \\emph{active agreements}: on both topic sets TS1 and TS2, either $S1 \\succ\\succ S2$ or $S2 \\succ\\succ S1$, averaged over the %d samples of topic sets. The bigger, the better. It corresponds to \\texttt{SSA} by \\citet{MoffatEtAl2012}, \\texttt{Success} by \\citet{UrbanoEtAl2013}, and \\texttt{Active Agreements} by \\citet{FaggioliFerro2020};\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{AD} \\emph{active disagreements}, $S1 \\succ\\succ S2$ on topic set TS1 but $S2 \\succ\\succ S1$ on topic set TS2; or $S2 \\succ\\succ S1$ on topic set TS1 but $S1 \\succ\\succ S2$ on topic set TS2, averaged over the %d samples of topic sets. The smaller, the better. It corresponds to \\texttt{SSD} by \\citet{MoffatEtAl2012}, \\texttt{Major Error} by \\citet{UrbanoEtAl2013}, and \\texttt{Active Disagrements} by \\citet{FaggioliFerro2020};\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{PA} \\emph{passive agreements}: on both topic set TS1 and TS2, $S1 \\succ S2$ or $S2 \\succ S1$, averaged over the %d samples of topic sets. The bigger, the better, since there is agreement and not everything can be significantly different; however, Active Agreements are better. It corresponds to \\texttt{NN} by \\citet{MoffatEtAl2012}, (somehow?) \\texttt{Non-significance} by \\citet{UrbanoEtAl2013}, and \\texttt{Passive Agreements} by \\citet{FaggioliFerro2020};\n', EXPERIMENT.analysis.topicSampling.samples);    
    fprintf(fid, '\\item \\textbf{PD} \\emph{passive disagreements}: $S1 \\succ S2$ on topic set TS1 but $S2 \\succ S1$ on topic set TS2; or $S2 \\succ S1$ on topic set TS1 but $S1 \\succ S2$ on topic set TS2, averaged over the %d samples of topic sets. The smaller, the better. It corresponds to (somehow?) \\texttt{NN} by \\citet{MoffatEtAl2012}, (somehow?) \\texttt{Non-significance} by \\citet{UrbanoEtAl2013};\n', EXPERIMENT.analysis.topicSampling.samples);    
    fprintf(fid, '\\item \\textbf{MA} \\emph{mixed agreements}: $S1 \\succ\\succ S2$ on topic set TS1 but $S1 \\succ S2$ on topic set TS2; or $S2 \\succ\\succ S1$ on topic set TS1 but $S2 \\succ S1$ on topic set TS2; $S1 \\succ S2$ on topic set TS1 but $S1 \\succ\\succ S2$ on topic set TS2; or $S2 \\succ S1$ on topic set TS1 but $S2 \\succ\\succ S1$ on topic set TS2;, averaged over the %d samples of topic sets. The smaller, the better. It corresponds to \\texttt{SN} + \\texttt{NS} by \\citet{MoffatEtAl2012}, \\texttt{Lack of Power} by \\citet{UrbanoEtAl2013}, and \\texttt{Passive Disagrements} by \\citet{FaggioliFerro2020}; \n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{MD} \\emph{mixed disagreements}: $S1 \\succ\\succ S2$ on topic set TS1 but $S2 \\succ S1$ on topic set TS2; or $S2 \\succ\\succ S1$ on topic set TS1 but $S1 \\succ S2$ on topic set TS2; or $S1 \\succ S2$ on topic set TS1 but $S2 \\succ\\succ S1$ on topic set TS2; or $S2 \\succ S1$ on topic set TS1 but $S1 \\succ\\succ S2$ on topic set TS2;, averaged over the %d samples of topic sets. The smaller, the better. It corresponds to \\texttt{Minor Error} by \\citet{UrbanoEtAl2013};\n', EXPERIMENT.analysis.topicSampling.samples);    
    fprintf(fid, '\\item \\textbf{PAA} \\emph{Proportion of Active Agreements}: $PAA = \\frac{2AA}{2AA + MA}$, i.e. the percentage agreement when both topic sets report significant differences, averaged over the %d samples of topic sets. It represents how many times approaches agree on two systems being significantly different on both topic sets with respect to the total number of times two systems have been found to be significantly different. The bigger  this ratio, the more stable is  the decision about which systems are significantly different, independently from the topic sets. It represents how many times an approach agrees on two systems being significantly different with respect to the total number of times two systems have been found to be significantly different; the bigger this ratio, the more stable is the decision about which systems are significantly different, independently from the topic samples. It corresponds to \\texttt{Ratio3} by \\citet{MoffatEtAl2012} and \\texttt{Proportion of Active Agreements} by \\citet{FaggioliFerro2020};\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{PPA} \\emph{Proportion of Passive Agreements}: $PPA = \\frac{2PA}{2PA + MA}$, i.e. the percentage agreement when both topic sets report no significant differences, averaged over the %d samples of topic sets. It represents how many times approaches agree on two systems being not significantly different on both topic sets with respect to  the total number of times two systems have been found to be not significantly different. The bigger this ratio, the more stable is  the decision about which systems are  not significantly different, independently from the topic sets.  It corresponds to \\texttt{Ratio4} by \\citet{MoffatEtAl2012} and \\texttt{Proportion of Passive Agreements} by \\citet{FaggioliFerro2020}.\n', EXPERIMENT.analysis.topicSampling.samples);
    fprintf(fid, '\\item \\textbf{New} \\emph{Proportion New}: $NEW = \\frac{AA}{AA + AD + MA + MD}$.\n');
    fprintf(fid, '\\end{itemize}\n');
    fprintf(fid, '\\end{itemize}\n\n');
    
    
    fprintf(fid, 'Figure~\\ref{fig:metrics} shows how the above measures are defined and computed.\n\n');
    
    fprintf(fid, '\\begin{figure}[p]\n');
    fprintf(fid, '\\centering.\n');
    fprintf(fid, '\\includegraphics[width=0.6\\textwidth]{metrics.pdf}\n');
    fprintf(fid, '\\caption{Definition and computation of the main measures.}\n');
    fprintf(fid, '\\label{fig:metrics}\n');
    fprintf(fid, '\\end{figure}\n');
           
    fprintf(fid, '\\setlength{\\LTcapwidth}{0.8\\textwidth} \n\n');
    
    
  
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
%         %##### FAKE MODELS NOT SIGNIFICANT
%         
%         fprintf(fid, '\\newpage\n\n\n');
%         
%         fprintf(fid, '{\\scriptsize \n\n');
%         
%         fprintf(fid, '\\begin{longtable}{|l|l||r||r|r|r||r|r|r||r|r|r|r|r|r|r|r|r|r|r|r|r|r|} \n');
%         %fprintf(fid, '\\scriptsize \n');
%         
%         
%         try
%             anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF12H0, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
%             anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF12H0, mid,  EXPERIMENT.track.(trackID).corpus, tsID, trackID);
%             serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
%                 'WorkspaceVarNames', {'cmp'}, ...
%                 'FileVarNames', {anovaCmpID});
%             
%             fprintf(fid, '\\caption{Summary of Fake ANOVA analyses ALWAYS NOT SIGNIFICANT for %s on track \\texttt{%s}; %d system runs and %d topics; %d system pairs analysed. \\texttt{prt\\_*\\_*} stands for topic sampling without replacement; \\texttt{rpl\\_*\\_*} stands for topic sampling with replacement; topic set sampling is repeated %d times.\\label{tab:md-%s-%s}} \\\\ \n', ...
%                 strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), strrep(trackID, '_', '\_'), ...
%                 cmp.factorA.info.runs, cmp.factorA.info.topics, ...
%                 cmp.factorA.info.pairs, EXPERIMENT.analysis.topicSampling.samples, ...
%                 mid, trackID);
%             
%             clear cmp;
%             
%         catch
%             fprintf(fid, '\\caption{Summary of Fake ANOVA analyses ALWAYS NOT SIGNIFICANT for %s on track \\texttt{%s}; %d system runs and %d topics; %d system pairs analysed. \\texttt{prt\\_*\\_*} stands for topic sampling without replacement; \\texttt{rpl\\_*\\_*} stands for topic sampling with replacement; topic set sampling is repeated %d times.\\label{tab:md-%s-%s}} \\\\ \n', ...
%                 strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), strrep(trackID, '_', '\_'), ...
%                 0, 0, ...
%                 0, EXPERIMENT.analysis.topicSampling.samples, ...
%                 mid, trackID);
%             
%         end
%        
%         fprintf(fid, '\\hline\\hline \n');
%         
%         fprintf(fid, '\\multicolumn{1}{|c|}{\\multirow{3}{*}{\\textbf{Model}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{\\textbf{Topic}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{$\\mathbf{\\tau}$}} & \\multicolumn{3}{c||}{\\multirow{2}{*}{ $\\mathbf{ \\hat{\\omega}^2 }$ }} & \\multicolumn{3}{c||}{\\multirow{2}{*}{\\textbf{Tukey CI}}} & \\multicolumn{14}{c|}{\\textbf{System Pairs}} \\\\ \n');
%         
%         fprintf(fid, '\\cline{10-23} \n');
%         
%         fprintf(fid, '   &  &  &  \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} &  & \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} & &  \\multicolumn{3}{c|}{\\textbf{Size}} & \\multicolumn{1}{c|}{\\textbf{Jaccard}} & \\multicolumn{1}{c|}{\\textbf{Overlap}} & \\multicolumn{1}{c|}{\\textbf{AA}} & \\multicolumn{1}{c|}{\\textbf{AD}} & \\multicolumn{1}{c|}{\\textbf{PA}} & \\multicolumn{1}{c|}{\\textbf{PD}} & \\multicolumn{1}{c|}{\\textbf{MA}} & \\multicolumn{1}{c|}{\\textbf{MD}}  & \\multicolumn{1}{c|}{\\textbf{PAA}}  & \\multicolumn{1}{c|}{\\textbf{PPA}} & \\multicolumn{1}{c|}{\\textbf{New}} \\\\ \n');
%         
%         fprintf(fid, '   &  \\multicolumn{1}{c||}{\\textbf{(TS1, TS2) size}}  & \\multicolumn{1}{c||}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}}  & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c|}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} \\\\ \n');
%         
%         fprintf(fid, '\\hline\\hline \n');
%         
%         fprintf(fid, '\\endfirsthead \n');
%         
%         
%         fprintf(fid, '\\caption[]{(continued)} \\\\ \n');
%         
%         fprintf(fid, '\\hline\\hline \n');
%         
%         fprintf(fid, '\\multicolumn{1}{|c|}{\\multirow{3}{*}{\\textbf{Model}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{\\textbf{Topic}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{$\\mathbf{\\tau}$}} & \\multicolumn{3}{c||}{\\multirow{2}{*}{ $\\mathbf{ \\hat{\\omega}^2 }$ }} & \\multicolumn{3}{c||}{\\multirow{2}{*}{\\textbf{Tukey CI}}} & \\multicolumn{14}{c|}{\\textbf{System Pairs}} \\\\ \n');
%         
%         fprintf(fid, '\\cline{10-23} \n');
%         
%         fprintf(fid, '   &  &  &  \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} &  & \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} & &  \\multicolumn{3}{c|}{\\textbf{Size}} & \\multicolumn{1}{c|}{\\textbf{Jaccard}} & \\multicolumn{1}{c|}{\\textbf{Overlap}} & \\multicolumn{1}{c|}{\\textbf{AA}} & \\multicolumn{1}{c|}{\\textbf{AD}} & \\multicolumn{1}{c|}{\\textbf{PA}} & \\multicolumn{1}{c|}{\\textbf{PD}} & \\multicolumn{1}{c|}{\\textbf{MA}} & \\multicolumn{1}{c|}{\\textbf{MD}}  & \\multicolumn{1}{c|}{\\textbf{PAA}}  & \\multicolumn{1}{c|}{\\textbf{PPA}} & \\multicolumn{1}{c|}{\\textbf{New}} \\\\ \n');
%         
%         fprintf(fid, '   &  \\multicolumn{1}{c||}{\\textbf{(TS1, TS2) size}}  & \\multicolumn{1}{c||}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}}  & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c|}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} \\\\ \n');
%         
%         fprintf(fid, '\\hline\\hline \n');
%         
%         fprintf(fid, '\\endhead \n');
%         
%               
%         tsIDNum = length(tsIDs);
%         
%         % process MDF12H0
%         for i = 1:tsIDNum
%             tsID = tsIDs{i};
%             
%             
%             try
%                 
%                 anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF12H0, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
%                 anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF12H0, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
%                 
%                 serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
%                     'WorkspaceVarNames', {'cmp'}, ...
%                     'FileVarNames', {anovaCmpID});
%                 
%                 if (i == 1)  % we are on the very first row of a block
%                     fprintf(fid, '\\multirow{%d}{*}{\\textbf{%s}} & \\texttt{%s} (%02d, %02d) & ', ...
%                         tsIDNum, upper(MDF12H0), ...
%                         strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
%                         cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
%                 else % we are on the first row of a tsID
%                     fprintf(fid, '                                & \\texttt{%s} (%02d, %02d)  & ', ...
%                         strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
%                         cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
%                 end
%                 
%                 fprintf(fid, '%3.2f $\\pm$ %3.2f & ', cmp.factorA.sts.tau.mean, cmp.factorA.sts.tau.ci);
%                 
%                 fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
%                     cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc1.ci, ...
%                     cmp.factorA.sts.soa.omega2p.tpc2.mean, cmp.factorA.sts.soa.omega2p.tpc2.ci);
%                 
%                 if (cmp.factorA.sts.soa.omega2p.p < EXPERIMENT.analysis.alpha.threshold)
%                     fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
%                 else
%                     fprintf(fid, '%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
%                 end
%                 
%                 fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
%                     cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc1.ci, ...
%                     cmp.factorA.sts.tukey.halfWidth.tpc2.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.ci);
%                 
%                 if (cmp.factorA.sts.tukey.halfWidth.p < EXPERIMENT.analysis.alpha.threshold)
%                     fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
%                 else
%                     fprintf(fid, '%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
%                 end
%                 
%                 fprintf(fid, '%.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & ', ...
%                     cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc1.ci, ...
%                     cmp.factorA.sts.pairs.sig.tpc2.mean, cmp.factorA.sts.pairs.sig.tpc2.ci);
%                 
%                 if (cmp.factorA.sts.pairs.sig.p < EXPERIMENT.analysis.alpha.threshold)
%                     fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.pairs.sig.p);
%                 else
%                     fprintf(fid, '%3.2f & ', cmp.factorA.sts.pairs.sig.p);
%                 end
%                 
%                 fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %3.2f \\\\ \n', ...
%                     cmp.factorA.sts.pairs.jaccard.mean, cmp.factorA.sts.pairs.jaccard.ci, ...
%                     cmp.factorA.sts.pairs.overlap.mean, cmp.factorA.sts.pairs.overlap.ci, ...
%                     cmp.factorA.sts.pairs.aa.mean, cmp.factorA.sts.pairs.aa.ci, ...
%                     cmp.factorA.sts.pairs.ad.mean, cmp.factorA.sts.pairs.ad.ci, ...
%                     cmp.factorA.sts.pairs.pa.mean, cmp.factorA.sts.pairs.pa.ci, ...
%                     cmp.factorA.sts.pairs.pd.mean, cmp.factorA.sts.pairs.pd.ci, ...
%                     cmp.factorA.sts.pairs.ma.mean, cmp.factorA.sts.pairs.ma.ci, ...
%                     cmp.factorA.sts.pairs.md.mean, cmp.factorA.sts.pairs.md.ci, ...
%                     cmp.factorA.sts.pairs.paa.mean, cmp.factorA.sts.pairs.paa.ci, ...
%                     cmp.factorA.sts.pairs.ppa.mean, cmp.factorA.sts.pairs.ppa.ci, ...
%                     cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean));
%                 
%             catch
%                 
%                 if (i == 1)  % we are on the very first row of a block
%                     fprintf(fid, '\\multirow{%d}{*}{\\textbf{%s}} & \\texttt{%s} (--, --) & ', ...
%                         tsIDNum, upper(MDF12H0), ...
%                         strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
%                 else % we are on the first row of a tsID
%                     fprintf(fid, '                                & \\texttt{%s} (--, --)  & ', ...
%                         strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
%                 end
%                 
%                 fprintf(fid, ' -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- \\\\ \n ');
%                 
%             end
%             
%             if (i ~= tsIDNum)
%                 fprintf(fid, '\\cline{2-23} \n');
%             end
%             
%             clear cmp;
%             
%         end % for topic set sample
%         fprintf(fid, '\\hline\\hline \n');
%         
%         
%         
%          % process MDF3H0
%         for j = 1:length(shardIDs)
%             shardID = shardIDs{j};
%             
%             for i = 1:tsIDNum
%                 tsID = tsIDs{i};
%                 
%                 
%                 try
%                     
%                     anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF3H0, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
%                     anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF3H0, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
%                     
%                                         
%                     serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
%                         'WorkspaceVarNames', {'cmp'}, ...
%                         'FileVarNames', {anovaCmpID});
%                     
%                     if (i == 1)
%                         fprintf(fid, '\\multirow{%d}{*}{ \\parbox{2.0cm}{ \\textbf{%s} \\\\ \\texttt{%s} }} & \\texttt{%s} (%02d, %02d) & ', ...
%                             tsIDNum, upper(MDF3H0), strrep(shardID, '_', '\_'), ...
%                             strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
%                             cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
%                     else
%                         fprintf(fid, '                                & \\texttt{%s} (%02d, %02d) & ', ...
%                             strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
%                             cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
%                     end
%                     
%                     
%                     fprintf(fid, '%3.2f $\\pm$ %3.2f & ', cmp.factorA.sts.tau.mean, cmp.factorA.sts.tau.ci);
%                     
%                     fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
%                         cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc1.ci, ...
%                         cmp.factorA.sts.soa.omega2p.tpc2.mean, cmp.factorA.sts.soa.omega2p.tpc2.ci);
%                     
%                     if (cmp.factorA.sts.soa.omega2p.p < EXPERIMENT.analysis.alpha.threshold)
%                         fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
%                     else
%                         fprintf(fid, '%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
%                     end
%                     
%                     fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
%                         cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc1.ci, ...
%                         cmp.factorA.sts.tukey.halfWidth.tpc2.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.ci);
%                     
%                     if (cmp.factorA.sts.tukey.halfWidth.p < EXPERIMENT.analysis.alpha.threshold)
%                         fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
%                     else
%                         fprintf(fid, '%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
%                     end
%                     
%                     fprintf(fid, '%.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & ', ...
%                         cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc1.ci, ...
%                         cmp.factorA.sts.pairs.sig.tpc2.mean, cmp.factorA.sts.pairs.sig.tpc2.ci);
%                     
%                     if (cmp.factorA.sts.pairs.sig.p < EXPERIMENT.analysis.alpha.threshold)
%                         fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.pairs.sig.p);
%                     else
%                         fprintf(fid, '%3.2f & ', cmp.factorA.sts.pairs.sig.p);
%                     end
%                     
%                     
%                     fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %3.2f \\\\ \n', ...
%                         cmp.factorA.sts.pairs.jaccard.mean, cmp.factorA.sts.pairs.jaccard.ci, ...
%                         cmp.factorA.sts.pairs.overlap.mean, cmp.factorA.sts.pairs.overlap.ci, ...
%                         cmp.factorA.sts.pairs.aa.mean, cmp.factorA.sts.pairs.aa.ci, ...
%                         cmp.factorA.sts.pairs.ad.mean, cmp.factorA.sts.pairs.ad.ci, ...
%                         cmp.factorA.sts.pairs.pa.mean, cmp.factorA.sts.pairs.pa.ci, ...
%                         cmp.factorA.sts.pairs.pd.mean, cmp.factorA.sts.pairs.pd.ci, ...
%                         cmp.factorA.sts.pairs.ma.mean, cmp.factorA.sts.pairs.ma.ci, ...
%                         cmp.factorA.sts.pairs.md.mean, cmp.factorA.sts.pairs.md.ci, ...
%                         cmp.factorA.sts.pairs.paa.mean, cmp.factorA.sts.pairs.paa.ci, ...
%                         cmp.factorA.sts.pairs.ppa.mean, cmp.factorA.sts.pairs.ppa.ci, ...
%                         cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean));
%                                        
%                 catch
%                     
%                     if (i == 1)
%                         fprintf(fid, '\\multirow{%d}{*}{ \\parbox{2.0cm}{\\textbf{%s} \\\\ \\texttt{%s}} } & \\texttt{%s} (--, --) & ',  ...
%                             tsIDNum, upper(MDF3H0), strrep(shardID, '_', '\_'), ...
%                             strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
%                     else
%                         fprintf(fid, '  & \\texttt{%s} (--, --) & ',  ...
%                             strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
%                     end
%                     
%                     fprintf(fid, ' -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & --   \\\\ \n');
%                     
%                 end
%                 
%                 
%                 if (i ~= tsIDNum)
%                     fprintf(fid, '\\cline{2-23} \n');
%                 end
%                 
%                 clear cmp;
%             end % for topic set sample
%             fprintf(fid, '\\hline\\hline \n');
%             
%             
%         end % for shard
%         
%         
%         
%         fprintf(fid, '\\end{longtable} \n\n');
%         
%         fprintf(fid, '} \n\n'); % close \footnootesize
%         


        %##### FAKE MODELS SIGNIFICANT 
        
        fprintf(fid, '\\newpage\n\n\n');
        
        fprintf(fid, '{\\scriptsize \n\n');
        
        fprintf(fid, '\\begin{longtable}{|l|l||r||r|r|r||r|r|r||r|r|r|r|r|r|r|r|r|r|r|r|r|r|} \n');
        %fprintf(fid, '\\scriptsize \n');
        
        
        try
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF12H1, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF12H1, mid,  EXPERIMENT.track.(trackID).corpus, tsID, trackID);
            serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                'WorkspaceVarNames', {'cmp'}, ...
                'FileVarNames', {anovaCmpID});
            
            fprintf(fid, '\\caption{Summary of Fake ANOVA analyses ALWAYS SIGNIFICANT for %s on track \\texttt{%s}; %d system runs and %d topics; %d system pairs analysed. \\texttt{prt\\_*\\_*} stands for topic sampling without replacement; \\texttt{rpl\\_*\\_*} stands for topic sampling with replacement; topic set sampling is repeated %d times.\\label{tab:md-%s-%s}} \\\\ \n', ...
                strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), strrep(trackID, '_', '\_'), ...
                cmp.factorA.info.runs, cmp.factorA.info.topics, ...
                cmp.factorA.info.pairs, EXPERIMENT.analysis.topicSampling.samples, ...
                mid, trackID);
            
            clear cmp;
            
        catch
            fprintf(fid, '\\caption{Summary of Fake ANOVA analyses ALWAYS SIGNIFICANT for %s on track \\texttt{%s}; %d system runs and %d topics; %d system pairs analysed. \\texttt{prt\\_*\\_*} stands for topic sampling without replacement; \\texttt{rpl\\_*\\_*} stands for topic sampling with replacement; topic set sampling is repeated %d times.\\label{tab:md-%s-%s}} \\\\ \n', ...
                strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), strrep(trackID, '_', '\_'), ...
                0, 0, ...
                0, EXPERIMENT.analysis.topicSampling.samples, ...
                mid, trackID);
            
        end
       
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\multirow{3}{*}{\\textbf{Model}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{\\textbf{Topic}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{$\\mathbf{\\tau}$}} & \\multicolumn{3}{c||}{\\multirow{2}{*}{ $\\mathbf{ \\hat{\\omega}^2 }$ }} & \\multicolumn{3}{c||}{\\multirow{2}{*}{\\textbf{Tukey CI}}} & \\multicolumn{14}{c|}{\\textbf{System Pairs}} \\\\ \n');
        
        fprintf(fid, '\\cline{10-23} \n');
        
        fprintf(fid, '   &  &  &  \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} &  & \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} & &  \\multicolumn{3}{c|}{\\textbf{Size}} & \\multicolumn{1}{c|}{\\textbf{Jaccard}} & \\multicolumn{1}{c|}{\\textbf{Overlap}} & \\multicolumn{1}{c|}{\\textbf{AA}} & \\multicolumn{1}{c|}{\\textbf{AD}} & \\multicolumn{1}{c|}{\\textbf{PA}} & \\multicolumn{1}{c|}{\\textbf{PD}} & \\multicolumn{1}{c|}{\\textbf{MA}} & \\multicolumn{1}{c|}{\\textbf{MD}}  & \\multicolumn{1}{c|}{\\textbf{PAA}}  & \\multicolumn{1}{c|}{\\textbf{PPA}} & \\multicolumn{1}{c|}{\\textbf{New}} \\\\ \n');
        
        fprintf(fid, '   &  \\multicolumn{1}{c||}{\\textbf{(TS1, TS2) size}}  & \\multicolumn{1}{c||}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}}  & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c|}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} \\\\ \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\endfirsthead \n');
        
        
        fprintf(fid, '\\caption[]{(continued)} \\\\ \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\multirow{3}{*}{\\textbf{Model}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{\\textbf{Topic}}} & \\multicolumn{1}{c||}{\\multirow{2}{*}{$\\mathbf{\\tau}$}} & \\multicolumn{3}{c||}{\\multirow{2}{*}{ $\\mathbf{ \\hat{\\omega}^2 }$ }} & \\multicolumn{3}{c||}{\\multirow{2}{*}{\\textbf{Tukey CI}}} & \\multicolumn{14}{c|}{\\textbf{System Pairs}} \\\\ \n');
        
        fprintf(fid, '\\cline{10-23} \n');
        
        fprintf(fid, '   &  &  &  \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} &  & \\multicolumn{1}{c}{} &  \\multicolumn{1}{c}{} & &  \\multicolumn{3}{c|}{\\textbf{Size}} & \\multicolumn{1}{c|}{\\textbf{Jaccard}} & \\multicolumn{1}{c|}{\\textbf{Overlap}} & \\multicolumn{1}{c|}{\\textbf{AA}} & \\multicolumn{1}{c|}{\\textbf{AD}} & \\multicolumn{1}{c|}{\\textbf{PA}} & \\multicolumn{1}{c|}{\\textbf{PD}} & \\multicolumn{1}{c|}{\\textbf{MA}} & \\multicolumn{1}{c|}{\\textbf{MD}}  & \\multicolumn{1}{c|}{\\textbf{PAA}}  & \\multicolumn{1}{c|}{\\textbf{PPA}} & \\multicolumn{1}{c|}{\\textbf{New}} \\\\ \n');
        
        fprintf(fid, '   &  \\multicolumn{1}{c||}{\\textbf{(TS1, TS2) size}}  & \\multicolumn{1}{c||}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}}  & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c||}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{TS1 avg.}} & \\multicolumn{1}{c|}{\\textbf{TS2 avg.}} & \\multicolumn{1}{c|}{\\textbf{p}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} & \\multicolumn{1}{c|}{\\textbf{avg.}} \\\\ \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\endhead \n');
        
              
        tsIDNum = length(tsIDs);
        
        % process MDF12H1
        for i = 1:tsIDNum
            tsID = tsIDs{i};
            
            
            try
                
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF12H1, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF12H1, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                    'WorkspaceVarNames', {'cmp'}, ...
                    'FileVarNames', {anovaCmpID});
                
                if (i == 1)  % we are on the very first row of a block
                    fprintf(fid, '\\multirow{%d}{*}{\\textbf{%s}} & \\texttt{%s} (%02d, %02d) & ', ...
                        tsIDNum, upper(MDF12H1), ...
                        strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
                        cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
                else % we are on the first row of a tsID
                    fprintf(fid, '                                & \\texttt{%s} (%02d, %02d)  & ', ...
                        strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
                        cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
                end
                
                fprintf(fid, '%3.2f $\\pm$ %3.2f & ', cmp.factorA.sts.tau.mean, cmp.factorA.sts.tau.ci);
                
                fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
                    cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc1.ci, ...
                    cmp.factorA.sts.soa.omega2p.tpc2.mean, cmp.factorA.sts.soa.omega2p.tpc2.ci);
                
                if (cmp.factorA.sts.soa.omega2p.p < EXPERIMENT.analysis.alpha.threshold)
                    fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
                else
                    fprintf(fid, '%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
                end
                
                fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
                    cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc1.ci, ...
                    cmp.factorA.sts.tukey.halfWidth.tpc2.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.ci);
                
                if (cmp.factorA.sts.tukey.halfWidth.p < EXPERIMENT.analysis.alpha.threshold)
                    fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
                else
                    fprintf(fid, '%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
                end
                
                fprintf(fid, '%.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & ', ...
                    cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc1.ci, ...
                    cmp.factorA.sts.pairs.sig.tpc2.mean, cmp.factorA.sts.pairs.sig.tpc2.ci);
                
                if (cmp.factorA.sts.pairs.sig.p < EXPERIMENT.analysis.alpha.threshold)
                    fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.pairs.sig.p);
                else
                    fprintf(fid, '%3.2f & ', cmp.factorA.sts.pairs.sig.p);
                end
                
                fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %3.2f \\\\ \n', ...
                    cmp.factorA.sts.pairs.jaccard.mean, cmp.factorA.sts.pairs.jaccard.ci, ...
                    cmp.factorA.sts.pairs.overlap.mean, cmp.factorA.sts.pairs.overlap.ci, ...
                    cmp.factorA.sts.pairs.aa.mean, cmp.factorA.sts.pairs.aa.ci, ...
                    cmp.factorA.sts.pairs.ad.mean, cmp.factorA.sts.pairs.ad.ci, ...
                    cmp.factorA.sts.pairs.pa.mean, cmp.factorA.sts.pairs.pa.ci, ...
                    cmp.factorA.sts.pairs.pd.mean, cmp.factorA.sts.pairs.pd.ci, ...
                    cmp.factorA.sts.pairs.ma.mean, cmp.factorA.sts.pairs.ma.ci, ...
                    cmp.factorA.sts.pairs.md.mean, cmp.factorA.sts.pairs.md.ci, ...
                    cmp.factorA.sts.pairs.paa.mean, cmp.factorA.sts.pairs.paa.ci, ...
                    cmp.factorA.sts.pairs.ppa.mean, cmp.factorA.sts.pairs.ppa.ci, ...
                    cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean));
                
            catch
                
                if (i == 1)  % we are on the very first row of a block
                    fprintf(fid, '\\multirow{%d}{*}{\\textbf{%s}} & \\texttt{%s} (--, --) & ', ...
                        tsIDNum, upper(MDF12H1), ...
                        strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
                else % we are on the first row of a tsID
                    fprintf(fid, '                                & \\texttt{%s} (--, --)  & ', ...
                        strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
                end
                
                fprintf(fid, ' -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- \\\\ \n ');
                
            end
            
            if (i ~= tsIDNum)
                fprintf(fid, '\\cline{2-23} \n');
            end
            
            clear cmp;
            
        end % for topic set sample
        fprintf(fid, '\\hline\\hline \n');
        
        
        
        % process MDF3H1
        for j = 1:length(shardIDs)
            shardID = shardIDs{j};
            
            for i = 1:tsIDNum
                tsID = tsIDs{i};
                
                
                try
                    
                    anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF3H1, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
                    anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF3H1, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
                    
                                        
                    serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
                        'WorkspaceVarNames', {'cmp'}, ...
                        'FileVarNames', {anovaCmpID});
                    
                    if (i == 1)
                        fprintf(fid, '\\multirow{%d}{*}{ \\parbox{2.0cm}{ \\textbf{%s} \\\\ \\texttt{%s} }} & \\texttt{%s} (%02d, %02d) & ', ...
                            tsIDNum, upper(MDF3H1), strrep(shardID, '_', '\_'), ...
                            strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
                            cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
                    else
                        fprintf(fid, '                                & \\texttt{%s} (%02d, %02d) & ', ...
                            strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'), ...
                            cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size);
                    end
                    
                    
                    fprintf(fid, '%3.2f $\\pm$ %3.2f & ', cmp.factorA.sts.tau.mean, cmp.factorA.sts.tau.ci);
                    
                    fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
                        cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc1.ci, ...
                        cmp.factorA.sts.soa.omega2p.tpc2.mean, cmp.factorA.sts.soa.omega2p.tpc2.ci);
                    
                    if (cmp.factorA.sts.soa.omega2p.p < EXPERIMENT.analysis.alpha.threshold)
                        fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
                    else
                        fprintf(fid, '%3.2f & ', cmp.factorA.sts.soa.omega2p.p);
                    end
                    
                    fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & ', ...
                        cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc1.ci, ...
                        cmp.factorA.sts.tukey.halfWidth.tpc2.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.ci);
                    
                    if (cmp.factorA.sts.tukey.halfWidth.p < EXPERIMENT.analysis.alpha.threshold)
                        fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
                    else
                        fprintf(fid, '%3.2f & ', cmp.factorA.sts.tukey.halfWidth.p);
                    end
                    
                    fprintf(fid, '%.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & ', ...
                        cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc1.ci, ...
                        cmp.factorA.sts.pairs.sig.tpc2.mean, cmp.factorA.sts.pairs.sig.tpc2.ci);
                    
                    if (cmp.factorA.sts.pairs.sig.p < EXPERIMENT.analysis.alpha.threshold)
                        fprintf(fid, '\\cellcolor{lightblue}%3.2f & ', cmp.factorA.sts.pairs.sig.p);
                    else
                        fprintf(fid, '%3.2f & ', cmp.factorA.sts.pairs.sig.p);
                    end
                    
                    
                    fprintf(fid, '%3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %.1f $\\pm$ %.1f & %3.2f $\\pm$ %3.2f & %3.2f $\\pm$ %3.2f & %3.2f \\\\ \n', ...
                        cmp.factorA.sts.pairs.jaccard.mean, cmp.factorA.sts.pairs.jaccard.ci, ...
                        cmp.factorA.sts.pairs.overlap.mean, cmp.factorA.sts.pairs.overlap.ci, ...
                        cmp.factorA.sts.pairs.aa.mean, cmp.factorA.sts.pairs.aa.ci, ...
                        cmp.factorA.sts.pairs.ad.mean, cmp.factorA.sts.pairs.ad.ci, ...
                        cmp.factorA.sts.pairs.pa.mean, cmp.factorA.sts.pairs.pa.ci, ...
                        cmp.factorA.sts.pairs.pd.mean, cmp.factorA.sts.pairs.pd.ci, ...
                        cmp.factorA.sts.pairs.ma.mean, cmp.factorA.sts.pairs.ma.ci, ...
                        cmp.factorA.sts.pairs.md.mean, cmp.factorA.sts.pairs.md.ci, ...
                        cmp.factorA.sts.pairs.paa.mean, cmp.factorA.sts.pairs.paa.ci, ...
                        cmp.factorA.sts.pairs.ppa.mean, cmp.factorA.sts.pairs.ppa.ci, ...
                        cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean));
                                       
                catch
                    
                    if (i == 1)
                        fprintf(fid, '\\multirow{%d}{*}{ \\parbox{2.0cm}{\\textbf{%s} \\\\ \\texttt{%s}} } & \\texttt{%s} (--, --) & ',  ...
                            tsIDNum, upper(MDF3H1), strrep(shardID, '_', '\_'), ...
                            strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
                    else
                        fprintf(fid, '  & \\texttt{%s} (--, --) & ',  ...
                            strrep(EXPERIMENT.analysis.topicSampling.(tsID).label, '_', '\_'));
                    end
                    
                    fprintf(fid, ' -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & --    \\\\ \n');
                    
                end
                
                
                if (i ~= tsIDNum)
                    fprintf(fid, '\\cline{2-23} \n');
                end
                
                clear cmp;
            end % for topic set sample
            fprintf(fid, '\\hline\\hline \n');
            
            
        end % for shard
        
        
        
        fprintf(fid, '\\end{longtable} \n\n');
        
        fprintf(fid, '} \n\n'); % close \footnootesize
        
        
        
        
    end % for measure
                                       
    
    fprintf(fid, '\\bibliographystyle{apalike} \n');
    fprintf(fid, '\\bibliography{bibliografia,zz-proceedings}\n\n');
    
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
                       
    fprintf('\n\n######## Total elapsed time for reporting summary Fake ANOVA analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
