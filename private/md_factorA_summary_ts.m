
%% md_factorA_summary_ts
% 
% Computes summary statistics of the two topic sets for the different ANOVA 
% models.

%% Synopsis
%
%   [cmp] = md_factorA_summary_ts(cmp)
%  
% *Parameters*
%
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%
% *Returns*
%
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [cmp] = md_factorA_summary_ts(cmp)

    % load common parameters
    common_parameters
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.soa.omega2p.tpc1|
    cmp.factorA.sts.soa.omega2p.tpc1.mean = nanmean(cmp.factorA.data.soa.omega2p.tpc1);
    cmp.factorA.sts.soa.omega2p.tpc1.std = nanstd(cmp.factorA.data.soa.omega2p.tpc1);
    cmp.factorA.sts.soa.omega2p.tpc1.ci = nanCI(cmp.factorA.data.soa.omega2p.tpc1, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.soa.omega2p.tpc2|
    cmp.factorA.sts.soa.omega2p.tpc2.mean = nanmean(cmp.factorA.data.soa.omega2p.tpc2);
    cmp.factorA.sts.soa.omega2p.tpc2.std = nanstd(cmp.factorA.data.soa.omega2p.tpc2);
    cmp.factorA.sts.soa.omega2p.tpc2.ci = nanCI(cmp.factorA.data.soa.omega2p.tpc2, EXPERIMENT.analysis.alpha.threshold);

    % the p-value of an unpaired t-test between |cmp.factorA.data.soa.omega2p.tpc1| 
    % and |cmp.factorA.data.soa.omega2p.tpc2| to tell whether the effect size in
    % the two topic sets is significantly different
    [~, cmp.factorA.sts.soa.omega2p.p] = EXPERIMENT.analysis.ttest(cmp.factorA.data.soa.omega2p.tpc1, cmp.factorA.data.soa.omega2p.tpc2);
    
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pwr.tpc1|
    cmp.factorA.sts.pwr.tpc1.mean = nanmean(cmp.factorA.data.pwr.tpc1);
    cmp.factorA.sts.pwr.tpc1.std = nanstd(cmp.factorA.data.pwr.tpc1);
    cmp.factorA.sts.pwr.tpc1.ci = nanCI(cmp.factorA.data.pwr.tpc1, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pwr.tpc2|
    cmp.factorA.sts.pwr.tpc2.mean = nanmean(cmp.factorA.data.pwr.tpc2);
    cmp.factorA.sts.pwr.tpc2.std = nanstd(cmp.factorA.data.pwr.tpc2);
    cmp.factorA.sts.pwr.tpc2.ci = nanCI(cmp.factorA.data.pwr.tpc2, EXPERIMENT.analysis.alpha.threshold);

    % the p-value of an unpaired t-test between |cmp.factorA.data.pwr.tpc1| 
    % and |cmp.factorA.data.pwr.tpc2| to tell whether the effect size in
    % the two topic sets is significantly different
    [~, cmp.factorA.sts.pwr.p] = EXPERIMENT.analysis.ttest(cmp.factorA.data.pwr.tpc1, cmp.factorA.data.pwr.tpc2);


    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tukey.halfWidth.tpc1|
    cmp.factorA.sts.tukey.halfWidth.tpc1.mean = nanmean(cmp.factorA.data.tukey.halfWidth.tpc1);
    cmp.factorA.sts.tukey.halfWidth.tpc1.std = nanstd(cmp.factorA.data.tukey.halfWidth.tpc1);
    cmp.factorA.sts.tukey.halfWidth.tpc1.ci = nanCI(cmp.factorA.data.tukey.halfWidth.tpc1, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tukey.halfWidth.tpc2|
    cmp.factorA.sts.tukey.halfWidth.tpc2.mean = nanmean(cmp.factorA.data.tukey.halfWidth.tpc2);
    cmp.factorA.sts.tukey.halfWidth.tpc2.std = nanstd(cmp.factorA.data.tukey.halfWidth.tpc2);
    cmp.factorA.sts.tukey.halfWidth.tpc2.ci = nanCI(cmp.factorA.data.tukey.halfWidth.tpc2, EXPERIMENT.analysis.alpha.threshold);
    
    % the p-value of an unpaired t-test between 
    % |cmp.factorA.data.tukey.halfWidth.tpc1| and 
    % |cmp.factorA.data.tukey.halfWidth.tpc2| to tell whether the Tukey
    % confidence interval in the two topic sets is significantly different
    [~, cmp.factorA.sts.tukey.halfWidth.p] = EXPERIMENT.analysis.ttest(cmp.factorA.data.tukey.halfWidth.tpc1, cmp.factorA.data.tukey.halfWidth.tpc2);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tau|
    cmp.factorA.sts.tau.mean = nanmean(cmp.factorA.data.tau);
    cmp.factorA.sts.tau.std = nanstd(cmp.factorA.data.tau);
    cmp.factorA.sts.tau.ci = nanCI(cmp.factorA.data.tau, EXPERIMENT.analysis.alpha.threshold);
       
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.sig.tpc1|
    cmp.factorA.sts.pairs.sig.tpc1.mean = nanmean(cmp.factorA.data.pairs.sig.tpc1);
    cmp.factorA.sts.pairs.sig.tpc1.std = nanstd(cmp.factorA.data.pairs.sig.tpc1);
    cmp.factorA.sts.pairs.sig.tpc1.ci = nanCI(cmp.factorA.data.pairs.sig.tpc1, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.sig.tpc2|
    cmp.factorA.sts.pairs.sig.tpc2.mean = nanmean(cmp.factorA.data.pairs.sig.tpc2);
    cmp.factorA.sts.pairs.sig.tpc2.std = nanstd(cmp.factorA.data.pairs.sig.tpc2);
    cmp.factorA.sts.pairs.sig.tpc2.ci = nanCI(cmp.factorA.data.pairs.sig.tpc2, EXPERIMENT.analysis.alpha.threshold);
    
    % the p-value of an unpaired t-test between 
    % |cmp.factorA.data.pairs.sig.tpc1| and |cmp.factorA.data.pairs.sig.tpc2| 
    % to tell whether the size of the significantly different systems sets 
    % in the two topic sets is significantly different
    [~, cmp.factorA.sts.pairs.sig.p] = EXPERIMENT.analysis.ttest(cmp.factorA.data.pairs.sig.tpc1, cmp.factorA.data.pairs.sig.tpc2);
                        
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.jaccard|
    cmp.factorA.sts.pairs.jaccard.mean = nanmean(cmp.factorA.data.pairs.jaccard);
    cmp.factorA.sts.pairs.jaccard.std = nanstd(cmp.factorA.data.pairs.jaccard);
    cmp.factorA.sts.pairs.jaccard.ci = nanCI(cmp.factorA.data.pairs.jaccard, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.overlap|
    cmp.factorA.sts.pairs.overlap.mean = nanmean(cmp.factorA.data.pairs.overlap);
    cmp.factorA.sts.pairs.overlap.std = nanstd(cmp.factorA.data.pairs.overlap);
    cmp.factorA.sts.pairs.overlap.ci = nanCI(cmp.factorA.data.pairs.overlap, EXPERIMENT.analysis.alpha.threshold);

    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.aa|
    cmp.factorA.sts.pairs.aa.mean = nanmean(cmp.factorA.data.pairs.aa);
    cmp.factorA.sts.pairs.aa.std = nanstd(cmp.factorA.data.pairs.aa);
    cmp.factorA.sts.pairs.aa.ci = nanCI(cmp.factorA.data.pairs.aa, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ad|
    cmp.factorA.sts.pairs.ad.mean = nanmean(cmp.factorA.data.pairs.ad);
    cmp.factorA.sts.pairs.ad.std = nanstd(cmp.factorA.data.pairs.ad);
    cmp.factorA.sts.pairs.ad.ci = nanCI(cmp.factorA.data.pairs.ad, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.pa|
    cmp.factorA.sts.pairs.pa.mean = nanmean(cmp.factorA.data.pairs.pa);
    cmp.factorA.sts.pairs.pa.std = nanstd(cmp.factorA.data.pairs.pa);
    cmp.factorA.sts.pairs.pa.ci = nanCI(cmp.factorA.data.pairs.pa, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.pd|
    cmp.factorA.sts.pairs.pd.mean = nanmean(cmp.factorA.data.pairs.pd);
    cmp.factorA.sts.pairs.pd.std = nanstd(cmp.factorA.data.pairs.pd);
    cmp.factorA.sts.pairs.pd.ci = nanCI(cmp.factorA.data.pairs.pd, EXPERIMENT.analysis.alpha.threshold);
        
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ma|
    cmp.factorA.sts.pairs.ma.mean = nanmean(cmp.factorA.data.pairs.ma);
    cmp.factorA.sts.pairs.ma.std = nanstd(cmp.factorA.data.pairs.ma);
    cmp.factorA.sts.pairs.ma.ci = nanCI(cmp.factorA.data.pairs.ma, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.md|
    cmp.factorA.sts.pairs.md.mean = nanmean(cmp.factorA.data.pairs.md);
    cmp.factorA.sts.pairs.md.std = nanstd(cmp.factorA.data.pairs.md);
    cmp.factorA.sts.pairs.md.ci = nanCI(cmp.factorA.data.pairs.md, EXPERIMENT.analysis.alpha.threshold);
    
        
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.raa|
    cmp.factorA.sts.pairs.raa.mean = nanmean(cmp.factorA.data.pairs.raa);
    cmp.factorA.sts.pairs.raa.std = nanstd(cmp.factorA.data.pairs.raa);
    cmp.factorA.sts.pairs.raa.ci = nanCI(cmp.factorA.data.pairs.raa, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rad|
    cmp.factorA.sts.pairs.rad.mean = nanmean(cmp.factorA.data.pairs.rad);
    cmp.factorA.sts.pairs.rad.std = nanstd(cmp.factorA.data.pairs.rad);
    cmp.factorA.sts.pairs.rad.ci = nanCI(cmp.factorA.data.pairs.rad, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rpa|
    cmp.factorA.sts.pairs.rpa.mean = nanmean(cmp.factorA.data.pairs.rpa);
    cmp.factorA.sts.pairs.rpa.std = nanstd(cmp.factorA.data.pairs.rpa);
    cmp.factorA.sts.pairs.rpa.ci = nanCI(cmp.factorA.data.pairs.rpa, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rpd|
    cmp.factorA.sts.pairs.rpd.mean = nanmean(cmp.factorA.data.pairs.rpd);
    cmp.factorA.sts.pairs.rpd.std = nanstd(cmp.factorA.data.pairs.rpd);
    cmp.factorA.sts.pairs.rpd.ci = nanCI(cmp.factorA.data.pairs.rpd, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rma|
    cmp.factorA.sts.pairs.rma.mean = nanmean(cmp.factorA.data.pairs.rma);
    cmp.factorA.sts.pairs.rma.std = nanstd(cmp.factorA.data.pairs.rma);
    cmp.factorA.sts.pairs.rma.ci = nanCI(cmp.factorA.data.pairs.rma, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rmd|
    cmp.factorA.sts.pairs.rmd.mean = nanmean(cmp.factorA.data.pairs.rmd);
    cmp.factorA.sts.pairs.rmd.std = nanstd(cmp.factorA.data.pairs.rmd);
    cmp.factorA.sts.pairs.rmd.ci = nanCI(cmp.factorA.data.pairs.rmd, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.paa|
    cmp.factorA.sts.pairs.paa.mean = nanmean(cmp.factorA.data.pairs.paa);
    cmp.factorA.sts.pairs.paa.std = nanstd(cmp.factorA.data.pairs.paa);
    cmp.factorA.sts.pairs.paa.ci = nanCI(cmp.factorA.data.pairs.paa, EXPERIMENT.analysis.alpha.threshold);
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ppa|
    cmp.factorA.sts.pairs.ppa.mean = nanmean(cmp.factorA.data.pairs.ppa);
    cmp.factorA.sts.pairs.ppa.std = nanstd(cmp.factorA.data.pairs.ppa);
    cmp.factorA.sts.pairs.ppa.ci = nanCI(cmp.factorA.data.pairs.ppa, EXPERIMENT.analysis.alpha.threshold);
    

end

