%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

hn = hostname();

if (strcmpi(hn, 'grace.dei.unipd.it'))    
    addpath(genpath('/ssd/data/ferro/matters/')) 
    EXPERIMENT.path.base = '/ssd/data/ferro/WSDM2022-FS/experiment/';
elseif (strcmpi(hn, 'hpcapri'))     
    addpath(genpath('/stor/progetti/p1022/p1022u00/matters/')) 
    EXPERIMENT.path.base = '/stor/progetti/p1022/p1022u00/WSDM2022-FS/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2022/WSDM/FS/experiment/';  
end


% The path for the corpora, i.e. text files containing the list of
% documents for a corpus
EXPERIMENT.path.corpus = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'corpus', filesep);

% The path for the shards, i.e. directories containing text files, 
% each one listing documents from a corpus according to some criterion
EXPERIMENT.path.shard = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'shard', filesep);

% The path for the topic, i.e. directories containing different samples of
% topics for each track
EXPERIMENT.path.topic = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'topic', filesep);

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'SIGIR 2021 FS';


%% Configuration for Corpora

EXPERIMENT.corpus.list = {'TIP'};
EXPERIMENT.corpus.number = length(EXPERIMENT.corpus.list);

% The full TIPSTER corpus
EXPERIMENT.corpus.TIP.id = 'TIP';
EXPERIMENT.corpus.TIP.name = 'TIPSTER, Disk 4-5 minus Congressional Record';
EXPERIMENT.corpus.TIP.size = 528155;


%% Configuration for Shards

EXPERIMENT.shard.list = {'TIP_RNDE_02', 'TIP_RNDE_03', 'TIP_RNDE_05'};
EXPERIMENT.shard.number = length(EXPERIMENT.shard.list);

% The total number of samples for each shard
EXPERIMENT.shard.sample = 1;

EXPERIMENT.shard.TIP_RNDE_02.id = 'TIP_RNDE_02';
EXPERIMENT.shard.TIP_RNDE_02.name = 'TIPSTER divided in 2 even size shards';
EXPERIMENT.shard.TIP_RNDE_02.corpus = 'TIP';
EXPERIMENT.shard.TIP_RNDE_02.shard = 2;
EXPERIMENT.shard.TIP_RNDE_02.ratio = repmat(1/EXPERIMENT.shard.TIP_RNDE_02.shard, 1, EXPERIMENT.shard.TIP_RNDE_02.shard);

EXPERIMENT.shard.TIP_RNDE_03.id = 'TIP_RNDE_03';
EXPERIMENT.shard.TIP_RNDE_03.name = 'TIPSTER divided in 3 even size shards';
EXPERIMENT.shard.TIP_RNDE_03.corpus = 'TIP';
EXPERIMENT.shard.TIP_RNDE_03.shard = 3;
EXPERIMENT.shard.TIP_RNDE_03.ratio = repmat(1/EXPERIMENT.shard.TIP_RNDE_03.shard, 1, EXPERIMENT.shard.TIP_RNDE_03.shard);

EXPERIMENT.shard.TIP_RNDE_05.id = 'TIP_RNDE_05';
EXPERIMENT.shard.TIP_RNDE_05.name = 'TIPSTER divided in 5 even size shards';
EXPERIMENT.shard.TIP_RNDE_05.corpus = 'TIP';
EXPERIMENT.shard.TIP_RNDE_05.shard = 5;
EXPERIMENT.shard.TIP_RNDE_05.ratio = repmat(1/EXPERIMENT.shard.TIP_RNDE_05.shard, 1, EXPERIMENT.shard.TIP_RNDE_05.shard);

% Returns the label of a shard in its split, given its index
EXPERIMENT.shard.getShardLabel = @(splitID, idx) ( EXPERIMENT.shard.(splitID).shardLabels{idx} ); 

% Returns the name of a shard in its split, given its index
EXPERIMENT.shard.getShardName = @(splitID, idx) ( EXPERIMENT.shard.(splitID).shardNames{idx} ); 


%% Configuration for participant runs

EXPERIMENT.participant.list = {'APL', 'FUB', 'HUM', 'ICL', 'JURU', 'MPI', 'NLPR', ...
                               'PIRC', 'POLY', 'SABIR', 'UOG', 'VTU', 'WDO'};
EXPERIMENT.participant.number = length(EXPERIMENT.participant.list);

% Runs of participant APL - Johns Hopkins University 
EXPERIMENT.participant.APL.id = 'APL';
EXPERIMENT.participant.APL.org = 'Johns Hopkins University';
EXPERIMENT.participant.APL.runs.list = {'apl04rsDw', 'apl04rsTDNfw', 'apl04rsTDNw5', 'apl04rsTs', 'apl04rsTw'};
EXPERIMENT.participant.APL.runs.number = length(EXPERIMENT.participant.APL.runs.list);
EXPERIMENT.participant.APL.track = 'T13';

% Runs of participant FUB - Fondazione Ugo Bordoni
EXPERIMENT.participant.FUB.id = 'FUB';
EXPERIMENT.participant.FUB.org = 'Fondazione Ugo Bordoni';
EXPERIMENT.participant.FUB.runs.list = {'fub04De', 'fub04Dg','fub04Dge', 'fub04T2ge', 'fub04TDNe', 'fub04TDNg', 'fub04TDNge', 'fub04Te', 'fub04Tg', 'fub04Tge'};
EXPERIMENT.participant.FUB.runs.number = length(EXPERIMENT.participant.FUB.runs.list);
EXPERIMENT.participant.FUB.track = 'T13';

% Runs of participant HUM - Hummingbird
EXPERIMENT.participant.HUM.id = 'HUM';
EXPERIMENT.participant.HUM.org = 'Hummingbird';
EXPERIMENT.participant.HUM.runs.list = {'humR04d4', 'humR04d4e5', 'humR04d5', 'humR04d5i', 'humR04d5m', 'humR04t1', 'humR04t1i', 'humR04t1m', 'humR04t5', 'humR04t5e1'};
EXPERIMENT.participant.HUM.runs.number = length(EXPERIMENT.participant.HUM.runs.list);
EXPERIMENT.participant.HUM.track = 'T13';

% Runs of participant ICL - Peking University
EXPERIMENT.participant.ICL.id = 'ICL';
EXPERIMENT.participant.ICL.org = 'Peking University';
EXPERIMENT.participant.ICL.runs.list = {'icl04pos2d', 'icl04pos2f', 'icl04pos2t', 'icl04pos2td', 'icl04pos48f', 'icl04pos7f', 'icl04pos7tap', 'icl04pos7tat', 'icl04pos7td'};
EXPERIMENT.participant.ICL.runs.number = length(EXPERIMENT.participant.ICL.runs.list);
EXPERIMENT.participant.ICL.track = 'T13';

% Runs of participant JURU - IBM Research Lab
EXPERIMENT.participant.JURU.id = 'JURU';
EXPERIMENT.participant.JURU.org = 'IBM Research Lab';
EXPERIMENT.participant.JURU.runs.list = {'JuruDes', 'JuruDesAggr', 'JuruDesLaMd', 'JuruDesQE', 'JuruDesSwQE', 'JuruDesTrSl', 'JuruTit', 'JuruTitDes', 'JuruTitSwDs', 'JuruTitSwQE'};
EXPERIMENT.participant.JURU.runs.number = length(EXPERIMENT.participant.JURU.runs.list);
EXPERIMENT.participant.JURU.track = 'T13';

% Runs of participant MPI - Max-Planck-Institute for Computer Science
EXPERIMENT.participant.MPI.id = 'MPI';
EXPERIMENT.participant.MPI.org = 'Max-Planck-Institute for Computer Science';
EXPERIMENT.participant.MPI.runs.list = {'mpi04r01', 'mpi04r02', 'mpi04r04', 'mpi04r05', 'mpi04r06', 'mpi04r07', 'mpi04r08', 'mpi04r09', 'mpi04r10', 'mpi04r11'};
EXPERIMENT.participant.MPI.runs.number = length(EXPERIMENT.participant.MPI.runs.list);
EXPERIMENT.participant.MPI.track = 'T13';

% Runs of participant NLPR - Chinese Academy of Sciences (CAS-NLPR) 
EXPERIMENT.participant.NLPR.id = 'NLPR';
EXPERIMENT.participant.NLPR.org = 'Chinese Academy of Sciences (CAS-NLPR) ';
EXPERIMENT.participant.NLPR.runs.list = {'NLPR04clus10', 'NLPR04clus9', 'NLPR04COMB', 'NLPR04LcA', 'NLPR04LMts', 'NLPR04NcA', 'NLPR04okall', 'NLPR04OKapi', 'NLPR04okdiv', 'NLPR04oktwo', 'NLPR04SemLM'};
EXPERIMENT.participant.NLPR.runs.number = length(EXPERIMENT.participant.NLPR.runs.list);
EXPERIMENT.participant.NLPR.track = 'T13';

% Runs of participant PIRC - Queens College, CUNY
EXPERIMENT.participant.PIRC.id = 'PIRC';
EXPERIMENT.participant.PIRC.org = 'Queens College, CUNY';
EXPERIMENT.participant.PIRC.runs.list = {'pircRB04d2', 'pircRB04d3', 'pircRB04d4', 'pircRB04d5', 'pircRB04t1', 'pircRB04t2', 'pircRB04t3', 'pircRB04t4', 'pircRB04td2', 'pircRB04td3'};
EXPERIMENT.participant.PIRC.runs.number = length(EXPERIMENT.participant.PIRC.runs.list);
EXPERIMENT.participant.PIRC.track = 'T13';

% Runs of participant POLY - Hong Kong Polytechnic University 
EXPERIMENT.participant.POLY.id = 'POLY';
EXPERIMENT.participant.POLY.org = 'Hong Kong Polytechnic University';
EXPERIMENT.participant.POLY.runs.list = {'polyudp2', 'polyudp4', 'polyudp5', 'polyudp6', 'polyutp1', 'polyutp3'};
EXPERIMENT.participant.POLY.runs.number = length(EXPERIMENT.participant.POLY.runs.list);
EXPERIMENT.participant.POLY.track = 'T13';

% Runs of participant SABIR - Sabir Research, Inc.
EXPERIMENT.participant.SABIR.id = 'SABIR';
EXPERIMENT.participant.SABIR.org = 'Sabir Research, Inc.';
EXPERIMENT.participant.SABIR.runs.list = {'SABIR04BA', 'SABIR04BD', 'SABIR04BT', 'SABIR04FA', 'SABIR04FD', 'SABIR04FT'};
EXPERIMENT.participant.SABIR.runs.number = length(EXPERIMENT.participant.SABIR.runs.list);
EXPERIMENT.participant.SABIR.track = 'T13';

% Runs of participant UOG - University of Glasgow
EXPERIMENT.participant.UOG.id = 'UOG';
EXPERIMENT.participant.UOG.org = 'University of Glasgow';
EXPERIMENT.participant.UOG.runs.list = {'uogRobDBase', 'uogRobDWR10', 'uogRobDWR5', 'uogRobLBase', 'uogRobLT', 'uogRobLWR10', 'uogRobLWR5', 'uogRobSBase', 'uogRobSWR10', 'uogRobSWR5'};
EXPERIMENT.participant.UOG.runs.number = length(EXPERIMENT.participant.UOG.runs.list);
EXPERIMENT.participant.UOG.track = 'T13';

% Runs of participant VTU - Virginia Tech
EXPERIMENT.participant.VTU.id = 'VTU';
EXPERIMENT.participant.VTU.org = 'Virginia Tech';
EXPERIMENT.participant.VTU.runs.list = {'vtumdesc', 'vtumlong252', 'vtumlong254', 'vtumlong344', 'vtumlong348', 'vtumlong432', 'vtumlong436', 'vtumtitle'};
EXPERIMENT.participant.VTU.runs.number = length(EXPERIMENT.participant.VTU.runs.list);
EXPERIMENT.participant.VTU.track = 'T13';

% Runs of participant WDO - Indiana University (Yang)
EXPERIMENT.participant.WDO.id = 'WDO';
EXPERIMENT.participant.WDO.org = 'Indiana University (Yang)';
EXPERIMENT.participant.WDO.runs.list = {'wdo25qla1', 'wdoqdn1', 'wdoqla1', 'wdoqsn1'};
EXPERIMENT.participant.WDO.runs.number = length(EXPERIMENT.participant.WDO.runs.list);
EXPERIMENT.participant.WDO.track = 'T13';

%% Configuration for Tracks

EXPERIMENT.track.list = {'T13', ...
                         'APL', 'FUB', 'HUM', 'ICL', 'JURU', 'MPI', 'NLPR', ...
                         'PIRC', 'POLY', 'SABIR', 'UOG', 'VTU', 'WDO'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);


% TREC 13, 2004, Robust
EXPERIMENT.track.T13.id = 'T13';
EXPERIMENT.track.T13.name = 'TREC 13, 2004, Robust';
EXPERIMENT.track.T13.corpus = 'TIP';
EXPERIMENT.track.T13.topics = 249;
EXPERIMENT.track.T13.runs = 110;
EXPERIMENT.track.T13.runLength = 1000;
EXPERIMENT.track.T13.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Robust/pool/qrels.robust2004.txt';
EXPERIMENT.track.T13.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13.pool.delimiter = 'space';
EXPERIMENT.track.T13.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T13.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Robust/runs';
EXPERIMENT.track.T13.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13.run.singlePrecision = true;
EXPERIMENT.track.T13.run.delimiter = 'tab';
EXPERIMENT.track.T13.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13.run.delimiter, 'Verbose', false);


% TREC 13, 2004, Robust - APL
EXPERIMENT.track.APL.id = 'APL';
EXPERIMENT.track.APL.name = ['TREC 13, 2004, Robust - Runs by participant APL, ' EXPERIMENT.participant.APL.org];
EXPERIMENT.track.APL.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.APL.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.APL.runs = EXPERIMENT.participant.APL.runs.number;
EXPERIMENT.track.APL.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - FUB
EXPERIMENT.track.FUB.id = 'FUB';
EXPERIMENT.track.FUB.name = ['TREC 13, 2004, Robust - Runs by participant FUB, ' EXPERIMENT.participant.FUB.org];
EXPERIMENT.track.FUB.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.FUB.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.FUB.runs = EXPERIMENT.participant.FUB.runs.number;
EXPERIMENT.track.FUB.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - HUM
EXPERIMENT.track.HUM.id = 'HUM';
EXPERIMENT.track.HUM.name = ['TREC 13, 2004, Robust - Runs by participant HUM, ' EXPERIMENT.participant.HUM.org];
EXPERIMENT.track.HUM.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.HUM.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.HUM.runs = EXPERIMENT.participant.HUM.runs.number;
EXPERIMENT.track.HUM.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - ICL
EXPERIMENT.track.ICL.id = 'ICL';
EXPERIMENT.track.ICL.name = ['TREC 13, 2004, Robust - Runs by participant ICL, ' EXPERIMENT.participant.ICL.org];
EXPERIMENT.track.ICL.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.ICL.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.ICL.runs = EXPERIMENT.participant.ICL.runs.number;
EXPERIMENT.track.ICL.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - JURU
EXPERIMENT.track.JURU.id = 'JURU';
EXPERIMENT.track.JURU.name = ['TREC 13, 2004, Robust - Runs by participant JURU, ' EXPERIMENT.participant.JURU.org];
EXPERIMENT.track.JURU.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.JURU.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.JURU.runs = EXPERIMENT.participant.JURU.runs.number;
EXPERIMENT.track.JURU.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - MPI
EXPERIMENT.track.MPI.id = 'MPI';
EXPERIMENT.track.MPI.name = ['TREC 13, 2004, Robust - Runs by participant MPI, ' EXPERIMENT.participant.MPI.org];
EXPERIMENT.track.MPI.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.MPI.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.MPI.runs = EXPERIMENT.participant.MPI.runs.number;
EXPERIMENT.track.MPI.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - NLPR
EXPERIMENT.track.NLPR.id = 'NLPR';
EXPERIMENT.track.NLPR.name = ['TREC 13, 2004, Robust - Runs by participant NLPR, ' EXPERIMENT.participant.NLPR.org];
EXPERIMENT.track.NLPR.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.NLPR.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.NLPR.runs = EXPERIMENT.participant.NLPR.runs.number;
EXPERIMENT.track.NLPR.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - PIRC
EXPERIMENT.track.PIRC.id = 'PIRC';
EXPERIMENT.track.PIRC.name = ['TREC 13, 2004, Robust - Runs by participant PIRC, ' EXPERIMENT.participant.PIRC.org];
EXPERIMENT.track.PIRC.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.PIRC.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.PIRC.runs = EXPERIMENT.participant.PIRC.runs.number;
EXPERIMENT.track.PIRC.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - POLY
EXPERIMENT.track.POLY.id = 'POLY';
EXPERIMENT.track.POLY.name = ['TREC 13, 2004, Robust - Runs by participant POLY, ' EXPERIMENT.participant.POLY.org];
EXPERIMENT.track.POLY.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.POLY.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.POLY.runs = EXPERIMENT.participant.POLY.runs.number;
EXPERIMENT.track.POLY.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - SABIR
EXPERIMENT.track.SABIR.id = 'SABIR';
EXPERIMENT.track.SABIR.name = ['TREC 13, 2004, Robust - Runs by participant SABIR, ' EXPERIMENT.participant.SABIR.org];
EXPERIMENT.track.SABIR.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.SABIR.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.SABIR.runs = EXPERIMENT.participant.SABIR.runs.number;
EXPERIMENT.track.SABIR.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - UOG
EXPERIMENT.track.UOG.id = 'UOG';
EXPERIMENT.track.UOG.name = ['TREC 13, 2004, Robust - Runs by participant UOG, ' EXPERIMENT.participant.UOG.org];
EXPERIMENT.track.UOG.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.UOG.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.UOG.runs = EXPERIMENT.participant.UOG.runs.number;
EXPERIMENT.track.UOG.runLength = EXPERIMENT.track.T13.runLength;

% TREC 13, 2004, Robust - VTU
EXPERIMENT.track.VTU.id = 'VTU';
EXPERIMENT.track.VTU.name = ['TREC 13, 2004, Robust - Runs by participant VTU, ' EXPERIMENT.participant.VTU.org];
EXPERIMENT.track.VTU.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.VTU.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.VTU.runs = EXPERIMENT.participant.VTU.runs.number;
EXPERIMENT.track.VTU.runLength = EXPERIMENT.track.T13.runLength;


% TREC 13, 2004, Robust - WDO
EXPERIMENT.track.WDO.id = 'WDO';
EXPERIMENT.track.WDO.name = ['TREC 13, 2004, Robust - Runs by participant WDO, ' EXPERIMENT.participant.WDO.org];
EXPERIMENT.track.WDO.corpus = EXPERIMENT.track.T13.corpus;
EXPERIMENT.track.WDO.topics = EXPERIMENT.track.T13.topics;
EXPERIMENT.track.WDO.runs = EXPERIMENT.participant.WDO.runs.number;
EXPERIMENT.track.WDO.runLength = EXPERIMENT.track.T13.runLength;

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 


%% Patterns for file names

% TXT - Pattern EXPERIMENT.path.base/corpus/<corpusID>.txt
EXPERIMENT.pattern.file.corpus = @(corpusID) sprintf('%1$s%2$s.txt', EXPERIMENT.path.corpus, corpusID);

% TXT - Pattern EXPERIMENT.path.shard/<shardID>/<shrID>.txt
EXPERIMENT.pattern.file.shard = @(shardID, shrID) sprintf('%1$s%2$s%3$s%4$s.txt', EXPERIMENT.path.shard, shardID, filesep, shrID);

% ALL - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.corpus = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% ALL - Pattern <path>/<trackID>/<shardID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.shard = @(path, trackID, shardID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s%3$s%5$s.%6$s', path, trackID, filesep, shardID, fileID, ext);

% MAT - Pattern EXPERIMENT.path.base/topic/<trackID>/<tsetID>.mat
EXPERIMENT.pattern.file.dataset.topic = @(trackID, tsetID) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.topic, trackID, tsetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.corpus = @(trackID, datasetID) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<shardID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.shard = @(trackID, shardID, datasetID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.dataset, trackID, shardID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.corpus = @(trackID, measureID) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<shardID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.shard = @(trackID, shardID, measureID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.measure, trackID, shardID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis.corpus = @(trackID, analysisID) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<shardID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis.shard = @(trackID, shardID, analysisID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.analysis, trackID, shardID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>.<ext>
EXPERIMENT.pattern.file.report = @(trackID, reportID, ext) EXPERIMENT.pattern.file.general.corpus(EXPERIMENT.path.report, trackID, reportID, ext);



%% Patterns for identifiers

% Pattern <shardID>_shr<shr>_s<smpl>
EXPERIMENT.pattern.identifier.shard.full =  @(shardID, shr, smpl) sprintf('%1$s_shr%2$03d_s%3$03d', shardID, shr, smpl);

% Pattern <shardID>_s<smpl>
EXPERIMENT.pattern.identifier.shard.short =  @(splitID, smpl) sprintf('%1$s_s%2$03d', splitID, smpl);

% Pattern pool_<corpusID>_<trackID>
% Pattern pool_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.pool = @(partID, trackID) sprintf('pool_%1$s_%2$s', partID, trackID);

% Pattern run_<corpusID>_<trackID>
% Pattern run_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.run = @(partID, trackID) sprintf('run_%1$s_%2$s', partID, trackID);

% Pattern <runID>_<detRatio><detMode>_<detCounter>
EXPERIMENT.pattern.identifier.detRun = @(runID, detRatio, detMode, detCounter) sprintf('%1$s_%2$03d%3$s_det%4$03d', runID, detRatio*100, detMode(1), detCounter);

% Pattern tset_<tsID>_<trackID>
EXPERIMENT.pattern.identifier.topicSampling.tsetID = @(tsID, trackID) sprintf('tset_%1$s_%2$s', tsID, trackID);

% Pattern tpc<set>_<tsID>_<trackID>
EXPERIMENT.pattern.identifier.topicSampling.tpcID = @(set, tsID, trackID) sprintf('tpc%1$d_%2$s_%3$s', set, tsID, trackID);

% Pattern <mid>_<corpusID>_<trackID>
% Pattern <mid>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(mid, partID, trackID) sprintf('%1$s_%2$s_%3$s', mid, partID, trackID);

% Pattern <mdID>_<type>_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(mdID, type, measureID, shardID, tsID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s_%5$s_%6$s', mdID, type, measureID, shardID, tsID, trackID);

% Pattern <mdID>_anova_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'anova', measureID, shardID, tsID, trackID);

% Pattern <mdID>_tbl_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'tbl', measureID, shardID, tsID, trackID);

% Pattern <mdID>_sts_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'sts', measureID, shardID, tsID, trackID);

% Pattern <mdID>_me_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'me', measureID, shardID, tsID, trackID);

% Pattern <mdID>_soa_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'soa', measureID, shardID, tsID, trackID);

% Pattern <mdID>_cmp_<measureID>_<tsID>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.anova.cmp =  @(mdID, measureID, shardID, tsID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'cmp', measureID, shardID, tsID, trackID);

% Pattern c<md>3
EXPERIMENT.pattern.identifier.cmd =  @(md, large) sprintf('c%1$s3', md);

% Pattern detStats_<detID>
EXPERIMENT.pattern.identifier.detStats =  @(detID) sprintf('detStats_%1$s', detID);

% Pattern trs_ <measureID>_<mdID>_<trackID>
EXPERIMENT.pattern.identifier.trs.corpus =  @(measureID, mdID, trackID) sprintf('trs_%1$s_%2$s_%3$s', measureID, mdID, trackID);

% Pattern trs_ <measureID>_<mdID>_<trackID>
EXPERIMENT.pattern.identifier.trs.shard =  @(measureID, mdID, shardID, trackID) sprintf('trs_%1$s_%2$s_%3$s_%4$s', measureID, mdID, shardID, trackID);

% Pattern pdf_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.pdf =  @(measureID, trackID) sprintf('pdf_%1$s_%2$s', measureID, trackID);

% Pattern kld_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.kld =  @(measureID, trackID) sprintf('kld_%1$s_%2$s', measureID, trackID);

% Pattern mdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.mdanalysis = @(trackID) sprintf('mdanalysis_%1$s', trackID);

% Pattern mdfanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.mdfanalysis = @(trackID) sprintf('mdfanalysis_%1$s', trackID);

% Pattern cmdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.cmdanalysis = @(trackID) sprintf('cmdanalysis_%1$s', trackID);


% Pattern dmdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.dmdanalysis = @(trackID) sprintf('dmdanalysis_%1$s', trackID);

% Pattern dcmdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.dcmdanalysis = @(trackID) sprintf('dcmdanalysis_%1$s', trackID);

% Pattern pmdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.pmdanalysis = @(trackID) sprintf('pmdanalysis_%1$s', trackID);

% Pattern pcmdanalysis_<trackID>
EXPERIMENT.pattern.identifier.report.pcmdanalysis = @(trackID) sprintf('pcmdanalysis_%1$s', trackID);




% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me2 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me2_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me3 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me3_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me4 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me4_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern trs_ <measureID>_<type>_<mdID>_<trackID>
EXPERIMENT.pattern.identifier.figure.trs.corpus =  @(measureID, type, mdID, trackID) sprintf('trs_%1$s_%2$s_%3$s_%4$s', measureID, type, mdID, trackID);

% Pattern trs_ <measureID>_<type>_<mdID>_<trackID>
EXPERIMENT.pattern.identifier.figure.trs.shard =  @(measureID, type, mdID, shardID, trackID) sprintf('trs_%1$s_%2$s_%3$s_%4$s_%5$s', measureID, type, mdID, shardID, trackID);

% Pattern mdc_ <measureID>_<type>_<mdID>_<trackID>
EXPERIMENT.pattern.identifier.figure.mdc =  @(measureID, splitID, trackID) sprintf('mdc_%1$s_%2$s_%3$s', measureID, splitID, trackID);

% Pattern pbp_ <measureID>
EXPERIMENT.pattern.identifier.figure.pbp =  @(measureID) sprintf('pbp_%1$s', measureID);



% Pattern shard_stats_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.stats.shard =  @(splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', 'shard', 'stats', splitID, trackID);



%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ap', 'ndcg', 'p10'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, runLength) averagePrecision(pool, runSet);

% Configuration for P@10
EXPERIMENT.measure.p10.id = 'p10';
EXPERIMENT.measure.p10.acronym = 'P@10';
EXPERIMENT.measure.p10.name = 'Precision at 10 Retrieved Documents';
EXPERIMENT.measure.p10.cutoffs = 10;
EXPERIMENT.measure.p10.compute = @(pool, runSet, runLength) precision(pool, runSet, 'Cutoffs', EXPERIMENT.measure.p10.cutoffs);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.compute = @(pool, runSet, runLength) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

EXPERIMENT.analysis.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.largeEffect.color = 'blue';

EXPERIMENT.analysis.label.subject = 'Topic';
EXPERIMENT.analysis.color.subject = rgb('FireBrick');

EXPERIMENT.analysis.label.factorA = 'System';
EXPERIMENT.analysis.color.factorA = rgb('RoyalBlue');

EXPERIMENT.analysis.label.factorB = 'Shard';
EXPERIMENT.analysis.color.factorB = rgb('ForestGreen');

% the number of times the sampling has to be repeated
EXPERIMENT.analysis.topicSampling.samples = 100;

% The list of the possible ways of sampling topic sets
EXPERIMENT.analysis.topicSampling.list = {'prt0202', 'prt0404', 'prt1010', 'prt2020','prt5050'};

EXPERIMENT.analysis.topicSampling.prt0202.id = 'prt0202';
EXPERIMENT.analysis.topicSampling.prt0202.description = 'It samples topics without replacement';
EXPERIMENT.analysis.topicSampling.prt0202.sampling = 'prt';
EXPERIMENT.analysis.topicSampling.prt0202.tpc1 = 0.02;
EXPERIMENT.analysis.topicSampling.prt0202.tpc2 = 0.02;
EXPERIMENT.analysis.topicSampling.prt0202.label = 'prt_02_02';

EXPERIMENT.analysis.topicSampling.prt0404.id = 'prt0404';
EXPERIMENT.analysis.topicSampling.prt0404.description = 'It samples topics without replacement';
EXPERIMENT.analysis.topicSampling.prt0404.sampling = 'prt';
EXPERIMENT.analysis.topicSampling.prt0404.tpc1 = 0.04;
EXPERIMENT.analysis.topicSampling.prt0404.tpc2 = 0.04;
EXPERIMENT.analysis.topicSampling.prt0404.label = 'prt_04_04';

EXPERIMENT.analysis.topicSampling.prt1010.id = 'prt1010';
EXPERIMENT.analysis.topicSampling.prt1010.description = 'It samples topics without replacement';
EXPERIMENT.analysis.topicSampling.prt1010.sampling = 'prt';
EXPERIMENT.analysis.topicSampling.prt1010.tpc1 = 0.10;
EXPERIMENT.analysis.topicSampling.prt1010.tpc2 = 0.10;
EXPERIMENT.analysis.topicSampling.prt1010.label = 'prt_10_10';

EXPERIMENT.analysis.topicSampling.prt2020.id = 'prt2020';
EXPERIMENT.analysis.topicSampling.prt2020.description = 'It samples topics without replacement. Same setup as \cite{UrbanoEtAl2013}.';
EXPERIMENT.analysis.topicSampling.prt2020.sampling = 'prt';
EXPERIMENT.analysis.topicSampling.prt2020.tpc1 = 0.20;
EXPERIMENT.analysis.topicSampling.prt2020.tpc2 = 0.20;
EXPERIMENT.analysis.topicSampling.prt2020.label = 'prt_20_20';

EXPERIMENT.analysis.topicSampling.prt5050.id = 'prt5050';
EXPERIMENT.analysis.topicSampling.prt5050.description = 'It samples topics without replacement';
EXPERIMENT.analysis.topicSampling.prt5050.sampling = 'prt';
EXPERIMENT.analysis.topicSampling.prt5050.tpc1 = 0.50;
EXPERIMENT.analysis.topicSampling.prt5050.tpc2 = 0.50;
EXPERIMENT.analysis.topicSampling.prt5050.label = 'prt_50_50';


% The list of the possible ANOVA analyses
EXPERIMENT.analysis.md.list = {'mdf0', 'mdf1', 'md1', 'md2', 'md3'};

% the sum of squares type to be used in the analyses
EXPERIMENT.analysis.md.sstype = 3;

% Fake models, detecting all the pairs as not significant
EXPERIMENT.analysis.mdf12h0.id = 'mdf12h0';
EXPERIMENT.analysis.mdf12h0.name = 'Fake Model 1 and 2, Not Significant';
EXPERIMENT.analysis.mdf12h0.description = 'Fake model 1 and 2, detecting all the pairs as not significant';
EXPERIMENT.analysis.mdf12h0.glmm = '$y_{j} = \ldots$';
EXPERIMENT.analysis.mdf12h0.model = [1];
EXPERIMENT.analysis.mdf12h0.compute = @(data, factorA) anovan(data, {factorA}, ...
        'Model', EXPERIMENT.analysis.mdf12h0.model, ...
        'VarNames', {EXPERIMENT.analysis.label.factorA}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.mdf12h0.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [1], 'Display', 'off');    
EXPERIMENT.analysis.mdf12h0.color.tpc1 = [8,48,107]/255; %rgb('RoyalBlue');
EXPERIMENT.analysis.mdf12h0.color.tpc2 = [33,113,181]/255; %rgb('SkyBlue');


EXPERIMENT.analysis.mdf12h1.id = 'mdf12h1';
EXPERIMENT.analysis.mdf12h1.name = 'Fake Model 1 and 2, Significant';
EXPERIMENT.analysis.mdf12h1.description = 'Fake model 1 and 2, detecting all the pairs as significant';
EXPERIMENT.analysis.mdf12h1.glmm = '$y_{j} = \ldots$';
EXPERIMENT.analysis.mdf12h1.model = [1];
EXPERIMENT.analysis.mdf12h1.compute = @(data, factorA) anovan(data, {factorA}, ...
        'Model', EXPERIMENT.analysis.mdf12h1.model, ...
        'VarNames', {EXPERIMENT.analysis.label.factorA}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.mdf12h1.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [1], 'Display', 'off');    
EXPERIMENT.analysis.mdf12h1.color.tpc1 = [8,48,107]/255; %rgb('RoyalBlue');
EXPERIMENT.analysis.mdf12h1.color.tpc2 = [33,113,181]/255; %rgb('SkyBlue');


EXPERIMENT.analysis.mdf3h0.id = 'mdf3h0';
EXPERIMENT.analysis.mdf3h0.name = 'Fake Model 3, Not Significant';
EXPERIMENT.analysis.mdf3h0.description = 'Fake model 3, detecting all the pairs as not significant';
EXPERIMENT.analysis.mdf3h0.glmm = '$y_{j} = \ldots$';
EXPERIMENT.analysis.mdf3h0.model = [1 0 0; ...
                                    0 1 0; ...
                                    0 0 1; ...
                                    1 1 0; ...
                                    0 1 1; ...
                                    1 0 1];
EXPERIMENT.analysis.mdf3h0.compute = @(data, subject, factorA, factorB) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.mdf3h0.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.mdf3h0.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');    
EXPERIMENT.analysis.mdf3h0.color.tpc1 = [8,48,107]/255; %rgb('RoyalBlue');
EXPERIMENT.analysis.mdf3h0.color.tpc2 = [33,113,181]/255; %rgb('SkyBlue');


EXPERIMENT.analysis.mdf3h1.id = 'mdf3h1';
EXPERIMENT.analysis.mdf3h1.name = 'Fake Model 3, Significant';
EXPERIMENT.analysis.mdf3h1.description = 'Fake model 3, detecting all the pairs as significant';
EXPERIMENT.analysis.mdf3h1.glmm = '$y_{j} = \ldots$';
EXPERIMENT.analysis.mdf3h1.model = [1 0 0; ...
                                    0 1 0; ...
                                    0 0 1; ...
                                    1 1 0; ...
                                    0 1 1; ...
                                    1 0 1];
EXPERIMENT.analysis.mdf3h1.compute = @(data, subject, factorA, factorB) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.mdf3h1.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.mdf3h1.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');    
EXPERIMENT.analysis.mdf3h1.color.tpc1 = [8,48,107]/255; %rgb('RoyalBlue');
EXPERIMENT.analysis.mdf3h1.color.tpc2 = [33,113,181]/255; %rgb('SkyBlue');


% System Effects on Whole Corpus
EXPERIMENT.analysis.md1.id = 'md1';
EXPERIMENT.analysis.md1.name = 'System Effects on Whole Corpus';
EXPERIMENT.analysis.md1.description = 'Crossed effects GLM on whole corpus: effects are systems';
EXPERIMENT.analysis.md1.glmm = '$y_{j} = \mu_{\cdot} + \alpha_j + \varepsilon_{j}$';
% the model =  System (factorA)
EXPERIMENT.analysis.md1.model = [1];
EXPERIMENT.analysis.md1.compute = @(data, factorA) anovan(data, {factorA}, ...
        'Model', EXPERIMENT.analysis.md1.model, ...
        'VarNames', {EXPERIMENT.analysis.label.factorA}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.md1.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [1], 'Display', 'off');    
EXPERIMENT.analysis.md1.color.tpc1 = [8,48,107]/255; %rgb('RoyalBlue');
EXPERIMENT.analysis.md1.color.tpc2 = [33,113,181]/255; %rgb('SkyBlue');


% Topic/System Effects on Whole Corpus
EXPERIMENT.analysis.md2.id = 'md2';
EXPERIMENT.analysis.md2.name = 'Topic/System Effects on Whole Corpus';
EXPERIMENT.analysis.md2.description = 'Crossed effects GLM on whole corpus: subjects are topics; effects are systems';
EXPERIMENT.analysis.md2.glmm = '$y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ij}$';
% the model = Topic (subject) + System (factorA)
EXPERIMENT.analysis.md2.model = [1 0; ... 
                                 0 1];
EXPERIMENT.analysis.md2.compute = @(data, subject, factorA) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.md2.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.md2.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');    
EXPERIMENT.analysis.md2.color.tpc1 = [0,68,27]/255; % rgb('ForestGreen');
EXPERIMENT.analysis.md2.color.tpc2 = [35,139,69]/255; %rgb('Teal');

% Topic/System/Shard Effects and Topic*System/System*Shard/Topic*Shard Interaction on Shards
EXPERIMENT.analysis.md3.id = 'md3';
EXPERIMENT.analysis.md3.name = 'Topic/System/Shard Effects and Topic*System/System*Shard/Topic*Shard Interactions on Shards';
EXPERIMENT.analysis.md3.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems and shards plus topic*system, system*shard, and topic*shard interactions';
EXPERIMENT.analysis.md3.glmm = '$y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\tau\alpha)_{ij} + (\alpha\beta)_{jk} + (\tau\beta)_{ik} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Shards (factorB)  +
%             Topic*System + System*Shard + Topic*Shard
EXPERIMENT.analysis.md3.model = [1 0 0; ...
                                 0 1 0; ...
                                 0 0 1; ...
                                 1 1 0; ...
                                 0 1 1; ...
                                 1 0 1];
EXPERIMENT.analysis.md3.compute = @(data, subject, factorA, factorB) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.md3.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', EXPERIMENT.analysis.md.sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.md3.multcompare = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');    
EXPERIMENT.analysis.md3.color.tpc1 = [103,0,13]/255; % rgb('FireBrick');        
EXPERIMENT.analysis.md3.color.tpc2 = [203,24,29]/255; %rgb('Red');        

% Correlation among two rankings of systems
EXPERIMENT.analysis.corr = @(x, y) corr(x(:), y(:), 'type', 'Kendall');

EXPERIMENT.analysis.ttest =  @(x, y) ttest2(x(:), y(:), 'alpha', EXPERIMENT.analysis.alpha.threshold, 'tail', 'both');

