%% hostname
% 
% Returns the name of the host on which Matlab is running.
%
%% Synopsis
%
%   [hn] = hostname()
%  
% *Parameters*
%
% Nothing.
%
% *Returns*
%
% * *|hn|* - the name of the host on which matlab is running
%

%% Information
% 
% * *Author*: <mailto:guglielmo.faggioli@phd.unipd.it Guglielmo Faggioli>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [hn] = hostname()
  [ret, hn] = system('hostname');
  if ret ~= 0
     if ispc
        hn = getenv('COMPUTERNAME');
     else
        hn = getenv('HOSTNAME');
     end
  end
  hn = strtrim(lower(hn));    
end
