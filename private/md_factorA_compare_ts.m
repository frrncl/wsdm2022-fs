
%% md_factorA_compare_ts
% 
% Compares the two topic sets for the different ANOVA analyses.

%% Synopsis
%
%   [cmp] = md_factorA_compare_ts(ts, me, soa, cmp)
%  
% *Parameters*
%
% * *|ts|* - the topic set sample
% * *|me|* - various information about the main effects for the system 
% factor.
% * *|soa|* - the strength-of-association according to different parameters 
% for the system factor.
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%
% *Returns*
%
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [cmp] = md_factorA_compare_ts(ts, me, soa, cmp)

    % load common parameters
    common_parameters

    % compute the correlation between the rankings of systems  in the two 
    % topic sets
    cmp.factorA.data.tau(ts) = EXPERIMENT.analysis.corr(me.factorA.mean.tpc1(ts, :), me.factorA.mean.tpc2(ts, :));

    
    % the effect size in the two topic sets
    cmp.factorA.data.soa.omega2p.tpc1(ts) = soa.omega2p.factorA.tpc1(ts);
    cmp.factorA.data.soa.omega2p.tpc2(ts) = soa.omega2p.factorA.tpc2(ts);

    % the power in the two topic sets
    cmp.factorA.data.pwr.tpc1(ts) = me.factorA.pwr.tpc1(ts);
    cmp.factorA.data.pwr.tpc2(ts) = me.factorA.pwr.tpc2(ts);


    % the Tukey confidence interval in the two topic sets
    cmp.factorA.data.tukey.halfWidth.tpc1(ts) = me.factorA.tukey.halfWidth.tpc1(ts);
    cmp.factorA.data.tukey.halfWidth.tpc2(ts) = me.factorA.tukey.halfWidth.tpc2(ts);
        
    % temp variables for holding the multcompare on the two topic sets
    tmp.mc.tpc1 = me.factorA.tukey.mc.tpc1{ts};
    tmp.mc.tpc2 = me.factorA.tukey.mc.tpc2{ts};
    
    assert(cmp.factorA.info.pairs == size(tmp.mc.tpc1, 1), 'Wrong number of pairs for topic set 1: %d instead of %d', size(tmp.mc.tpc1, 1), cmp.factorA.info.pairs);
    assert(cmp.factorA.info.pairs == size(tmp.mc.tpc2, 1), 'Wrong number of pairs for topic set 2: %d instead of %d', size(tmp.mc.tpc2, 1), cmp.factorA.info.pairs);
    
    
    % find the significantly different pairs in the two topic sets
    % NB column 6 in mc contains the p.value
    tmp.sig.tpc1 = tmp.mc.tpc1(:, 6) <= cmp.factorA.info.alpha;    
    tmp.sig.tpc2 = tmp.mc.tpc2(:, 6) <= cmp.factorA.info.alpha;
    
    % compute the size of the significantly different systems set in the 
    % two topic sets
    cmp.factorA.data.pairs.sig.tpc1(ts) = sum(tmp.sig.tpc1);
    cmp.factorA.data.pairs.sig.tpc2(ts) = sum(tmp.sig.tpc2);
    
    % system S1 is significantly better than system S2
    % Column 4 in mc contains the difference between the mean of S1 and S2
    tmp.s1SBs2.tpc1 = (tmp.mc.tpc1(:, 4) > 0) & tmp.sig.tpc1;
    tmp.s1SBs2.tpc2 = (tmp.mc.tpc2(:, 4) > 0) & tmp.sig.tpc2;
    
    % system S2 is significantly better than system S1
    % Column 4 in mc contains the difference between the mean of S1 and S2
    tmp.s2SBs1.tpc1 = (tmp.mc.tpc1(:, 4) < 0) & tmp.sig.tpc1;
    tmp.s2SBs1.tpc2 = (tmp.mc.tpc2(:, 4) < 0) & tmp.sig.tpc2;
    
    % system S1 is not significantly better than system S2
    % Column 4 in mc contains the difference between the mean of S1 and S2
    tmp.s1NBs2.tpc1 = (tmp.mc.tpc1(:, 4) > 0) & ~tmp.sig.tpc1;
    tmp.s1NBs2.tpc2 = (tmp.mc.tpc2(:, 4) > 0) & ~tmp.sig.tpc2;
    
    % system S2 is not significantly better than system S1
    % Column 4 in mc contains the difference between the mean of S1 and S2
    tmp.s2NBs1.tpc1 = (tmp.mc.tpc1(:, 4) <= 0) & ~tmp.sig.tpc1;
    tmp.s2NBs1.tpc2 = (tmp.mc.tpc2(:, 4) <= 0) & ~tmp.sig.tpc2;
    
    % ############## FIX POSSIBLY HORRIBLE BUG IN MULTCOMPARE ##############
    % When two systems have exactly the same scores for all the topics, the
    % mean difference is (correctly) 0 but the p-value is incorrectly 0; it
    % should be 1 since the two systems are NOT significantly different!
    %
    % The above cases do not account for this error.
    %
    % Here we fix it by finding when two systems are deemend significantly
    % different and their p-values is 0 and adding these systems to S2NBS1
    tmp.fixed.tpc1 = (tmp.mc.tpc1(:, 4) == 0) & (tmp.mc.tpc1(:, 6) == 0);
    tmp.fixed.tpc2 = (tmp.mc.tpc2(:, 4) == 0) & (tmp.mc.tpc2(:, 6) == 0);

    tmp.s2NBs1.tpc1(tmp.fixed.tpc1) = tmp.fixed.tpc1(tmp.fixed.tpc1);
    tmp.s2NBs1.tpc2(tmp.fixed.tpc2) = tmp.fixed.tpc2(tmp.fixed.tpc2);
    % ######################################################################
    
    
    % count how many times system S1 is significantly better than system S2
    cmp.factorA.data.pairs.s1SBs2.tpc1(ts) = sum(tmp.s1SBs2.tpc1);
    cmp.factorA.data.pairs.s1SBs2.tpc2(ts) = sum(tmp.s1SBs2.tpc2);
    
    % count how many times system S2 is significantly better than system S1
    cmp.factorA.data.pairs.s2SBs1.tpc1(ts) = sum(tmp.s2SBs1.tpc1);
    cmp.factorA.data.pairs.s2SBs1.tpc2(ts) = sum(tmp.s2SBs1.tpc2);
    
    % count how many times system S1 is not significantly better than system S2
    cmp.factorA.data.pairs.s1NBs2.tpc1(ts) = sum(tmp.s1NBs2.tpc1);
    cmp.factorA.data.pairs.s1NBs2.tpc2(ts) = sum(tmp.s1NBs2.tpc2);
    
    % count how many times system S2 is not significantly better than system S1
    cmp.factorA.data.pairs.s2NBs1.tpc1(ts) = sum(tmp.s2NBs1.tpc1);
    cmp.factorA.data.pairs.s2NBs1.tpc2(ts) = sum(tmp.s2NBs1.tpc2);
    
    % perform check only when we are not working with the fake models
    if ( ~startsWith(cmp.factorA.info.tpc1.md, 'mdf') )
        
        assert(cmp.factorA.info.pairs == cmp.factorA.data.pairs.s1SBs2.tpc1(ts) + ...
            cmp.factorA.data.pairs.s2SBs1.tpc1(ts) + cmp.factorA.data.pairs.s1NBs2.tpc1(ts) + ...
            cmp.factorA.data.pairs.s2NBs1.tpc1(ts), ...
            'Topic set 1, sample %d. s1SBs2 + s2SBs1 + s1NBs2 + s2NBs1 should be equal to the total number of pairs: %d instead of %d', ...
            ts, cmp.factorA.data.pairs.s1SBs2.tpc1(ts) + ...
            cmp.factorA.data.pairs.s2SBs1.tpc1(ts) + cmp.factorA.data.pairs.s1NBs2.tpc1(ts) + ...
            cmp.factorA.data.pairs.s2NBs1.tpc1(ts), cmp.factorA.info.pairs);
        
    end
    
    % perform check only when we are not working with the fake models
    if ( ~startsWith(cmp.factorA.info.tpc2.md, 'mdf') )
        assert(cmp.factorA.info.pairs == cmp.factorA.data.pairs.s1SBs2.tpc2(ts) + ...
            cmp.factorA.data.pairs.s2SBs1.tpc2(ts) + cmp.factorA.data.pairs.s1NBs2.tpc2(ts) + ...
            cmp.factorA.data.pairs.s2NBs1.tpc2(ts), ...
            'Topic set 2, sample %d. s1SBs2 + s2SBs1 + s1NBs2 + s2NBs1 should be equal to the total number of pairs: %d instead of %d', ...
            ts, cmp.factorA.data.pairs.s1SBs2.tpc2(ts) + ...
            cmp.factorA.data.pairs.s2SBs1.tpc2(ts) + cmp.factorA.data.pairs.s1NBs2.tpc2(ts) + ...
            cmp.factorA.data.pairs.s2NBs1.tpc2(ts), cmp.factorA.info.pairs);
    end
    
    
    % compute the Jaccard coefficient
    cmp.factorA.data.pairs.jaccard(ts) = sum(tmp.sig.tpc1 & tmp.sig.tpc2) / ...
        sum(tmp.sig.tpc1 | tmp.sig.tpc2);
    
    % compute the Overlap coefficient
    cmp.factorA.data.pairs.overlap(ts) = sum(tmp.sig.tpc1 & tmp.sig.tpc2) /...
        min(sum(tmp.sig.tpc1), sum(tmp.sig.tpc2));
    
    
    % the active agreements
    tmp.aa  = (tmp.s1SBs2.tpc1 & tmp.s1SBs2.tpc2) | ...
        (tmp.s2SBs1.tpc1 & tmp.s2SBs1.tpc2) ;
       
    % the active disagreements
    tmp.ad =  (tmp.s1SBs2.tpc1 & tmp.s2SBs1.tpc2) | ...
        (tmp.s2SBs1.tpc1 & tmp.s1SBs2.tpc2) ;
    
    % the passive agreements
    tmp.pa = (tmp.s1NBs2.tpc1 & tmp.s1NBs2.tpc2) | ...
        (tmp.s2NBs1.tpc1 & tmp.s2NBs1.tpc2) ;
        
    % the passive disagreements
    tmp.pd = (tmp.s1NBs2.tpc1 & tmp.s2NBs1.tpc2) | ...
            (tmp.s2NBs1.tpc1 & tmp.s1NBs2.tpc2);
        
    % the mixed agreements
    tmp.ma =  (tmp.s1SBs2.tpc1 & tmp.s1NBs2.tpc2) | ...
        (tmp.s2SBs1.tpc1 & tmp.s2NBs1.tpc2) | ...
        (tmp.s1NBs2.tpc1 & tmp.s1SBs2.tpc2) | ...
        (tmp.s2NBs1.tpc1 & tmp.s2SBs1.tpc2) ;
    
    % the mixed disagreements
    tmp.md = (tmp.s1SBs2.tpc1 & tmp.s2NBs1.tpc2) | ...
        (tmp.s2SBs1.tpc1 & tmp.s1NBs2.tpc2) | ...
        (tmp.s1NBs2.tpc1 & tmp.s2SBs1.tpc2) | ...
        (tmp.s2NBs1.tpc1 & tmp.s1SBs2.tpc2);
        
    
    % compute the active agreements
    cmp.factorA.data.pairs.aa(ts)  = sum(tmp.aa);
       
    % compute the active disagreements
    cmp.factorA.data.pairs.ad(ts) = sum(tmp.ad);
    
    % compute the passive agreements
    cmp.factorA.data.pairs.pa(ts) = sum(tmp.pa);
    
    % compute the passive disagreements
    cmp.factorA.data.pairs.pd(ts) = sum(tmp.pd);
    
    % compute the mixed agreements
    cmp.factorA.data.pairs.ma(ts) = sum(tmp.ma);
    
    % compute the mixed disagreements
    cmp.factorA.data.pairs.md(ts) = sum(tmp.md);
    

    % perform check only when we are not working with the fake models
    if ( ~startsWith(cmp.factorA.info.tpc1.md, 'mdf') && ~startsWith(cmp.factorA.info.tpc2.md, 'mdf') )
                
        assert(cmp.factorA.info.pairs == cmp.factorA.data.pairs.aa(ts) + ...
            cmp.factorA.data.pairs.ad(ts) + cmp.factorA.data.pairs.pa(ts) + ...
            cmp.factorA.data.pairs.pd(ts) + cmp.factorA.data.pairs.ma(ts) + ...
            cmp.factorA.data.pairs.md(ts), ...
            'Sample %d. AA + AD + PA + PD + MA + MD should be equal to the total number of pairs: %d instead of %d', ...
            ts, cmp.factorA.data.pairs.aa(ts) + ...
            cmp.factorA.data.pairs.ad(ts) + cmp.factorA.data.pairs.pa(ts) + ...
            cmp.factorA.data.pairs.pd(ts) + cmp.factorA.data.pairs.ma(ts) + ...
            cmp.factorA.data.pairs.md(ts), cmp.factorA.info.pairs);
    end
          
    % compute the rate of active agreements
    cmp.factorA.data.pairs.raa(ts) = cmp.factorA.data.pairs.aa(ts) / cmp.factorA.info.pairs;
    
    % compute the rate of active disagreements
    cmp.factorA.data.pairs.rad(ts) = cmp.factorA.data.pairs.ad(ts) / cmp.factorA.info.pairs;
    
    % compute the rate of passive agreements
    cmp.factorA.data.pairs.rpa(ts) = cmp.factorA.data.pairs.pa(ts) / cmp.factorA.info.pairs;
    
    % compute the rate of passive disagreements
    cmp.factorA.data.pairs.rpd(ts) = cmp.factorA.data.pairs.pd(ts) / cmp.factorA.info.pairs;
    
    % compute the rate of mixed agreements
    cmp.factorA.data.pairs.rma(ts) = cmp.factorA.data.pairs.ma(ts) / cmp.factorA.info.pairs;
    
    % compute the rate of mixed disagreements
    cmp.factorA.data.pairs.rmd(ts) = cmp.factorA.data.pairs.md(ts) / cmp.factorA.info.pairs;
    
    
    % compute the proportion of active agreements
    cmp.factorA.data.pairs.paa(ts) = 2*cmp.factorA.data.pairs.aa(ts) / ...
        (2*cmp.factorA.data.pairs.aa(ts) +  cmp.factorA.data.pairs.ma(ts));

    % compute the proportion of passive agreements
    cmp.factorA.data.pairs.ppa(ts) = 2*cmp.factorA.data.pairs.pa(ts)  / ...
        (2*cmp.factorA.data.pairs.pa(ts) + cmp.factorA.data.pairs.ma(ts));        

end

