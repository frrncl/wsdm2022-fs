
%% cmd_factorA_compare_ts
% 
% Compares the two topic sets across different ANOVA analyses.

%% Synopsis
%
%   [me, soa, cmp] = cmd_factorA_compare_ts(tpc1, tpc2, ts, md12_me, md12_soa, md3_me, md3_soa, me, soa, cmp)
%  
% *Parameters*
%
% * *|tpc1|* - the identifier of the first topic set
% * *|tpc2|* - the identifier of the second topic  set
% * *|ts|* - the topic set sample
% * *|md12_me|* - various information about the main effects for the system 
% factor for either MD1 or MD2.
% * *|md12_soa|* - various information about the effect size for the system 
% factor for either MD1 or MD2.
% * *|md3_me|* - various information about the main effects for the system 
% factor for either MD3.
% * *|md3_soa|* - various information about the effect size for the system 
% factor for either MD3.
% * *|me|* - various information about the main effects for the system 
% factor for either MD1 or MD2 and MD3.
% * *|soa|* - various information about the comparison between the effect 
% size on the two topic sets.
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%
% *Returns*
%
% * *|me|* - various information about the main effects for the system 
% factor for either MD1 or MD2 and MD3.
% * *|soa|* - various information about the comparison between the effect 
% size on the two topic sets.
% * *|cmp|* - various information about the comparison between the two
% topic sets.
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [me, soa, cmp] = cmd_factorA_compare_ts(tpc1, tpc2, ts, md12_me, md12_soa, md3_me, md3_soa, me, soa, cmp)

    % copy the effect size from MD1/2 into tpc1 and MD3 into tpc2
    soa.omega2p.factorA.tpc1(ts) = md12_soa.omega2p.factorA.(tpc1)(ts);
    soa.omega2p.factorA.tpc2(ts) = md3_soa.omega2p.factorA.(tpc2)(ts);
    
    % copy the main effect from MD1/2 into tpc1 and MD3 into tpc2
    me.factorA.mean.tpc1(ts, :) = md12_me.factorA.mean.(tpc1)(ts, :);
    me.factorA.mean.tpc2(ts, :) = md3_me.factorA.mean.(tpc2)(ts, :);
        
    % copy the Tukey multcompare from MD1/2 into tpc1 and MD3 into tpc2
    me.factorA.tukey.mc.tpc1{ts} = md12_me.factorA.tukey.mc.(tpc1){ts};
    me.factorA.tukey.mc.tpc2{ts} = md3_me.factorA.tukey.mc.(tpc2){ts};
    
    % copy the Tukey halfWidth CI from MD1/2 into tpc1 and MD3 into tpc2
    me.factorA.tukey.halfWidth.tpc1(ts) = md12_me.factorA.tukey.halfWidth.(tpc1)(ts);
    me.factorA.tukey.halfWidth.tpc2(ts) = md3_me.factorA.tukey.halfWidth.(tpc2)(ts);
    
    % compute the Tukey CI for MD1/2 
    me.factorA.tukey.ci.tpc1(ts, :, 1) = me.factorA.mean.tpc1(ts, :) - me.factorA.tukey.halfWidth.tpc1(ts);
    me.factorA.tukey.ci.tpc1(ts, :, 2) = me.factorA.mean.tpc1(ts, :) + me.factorA.tukey.halfWidth.tpc1(ts);
    
    % compute the Tukey CI for MD3
    me.factorA.tukey.ci.tpc2(ts, :, 1) = me.factorA.mean.tpc2(ts, :) - me.factorA.tukey.halfWidth.tpc2(ts);
    me.factorA.tukey.ci.tpc2(ts, :, 2) = me.factorA.mean.tpc2(ts, :) + me.factorA.tukey.halfWidth.tpc2(ts);
    
    % perform the actual comparison
    [cmp] = md_factorA_compare_ts(ts, me, soa, cmp);
    
end

