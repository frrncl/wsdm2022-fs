%%  layout_anova_data
% 
% Prepare measures for the subsequent ANOVA analyses. 
%
%% Synopsis
%
%   [data, subject, factorA, factorB] = layout_anova_data(measures, tpc1, tpc2, T, R, S, N)
%  
% *Parameters*
%
% * *|measures|* - a cell array of measure matrixes, one for each shard.
% * *|tpc1|* - the topics to besampled for the first topic set.
% * *|tpc2|* - the topics to besampled for the second topic set.
% * *|T|* - the total number of topics in the topic sets.
% * *|R|* - the total number of runs.
% * *|S|* - the total number of shards.
% * *|N|* - the total number of elements.
%
% *Returns*
%
% * *|data|* - the data vector, i.e. the |measures| properly laid out for
% ANOVA.
% * *|subject|* - the labels for the subjects, i.e. topics, in the |data|
% vector.
% * *|factorA|* - the labels for the factorA, i.e. systems, in the |data|
% vector.
% * *|factorB|* - the labels for the factorB, i.e. shards, in the |data|
% vector.
%
% Each of these returned variables, apart |R| and |S|, is a struct whose 
% fields |tpc1| and |tpc2| correspond to the values of those variables for 
% the first and thesecond topic set, respectively.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [data, subject, factorA, factorB] = layout_anova_data(measures, tpc1, tpc2, T, R, S, N)

    persistent SHR_ID;
    
    if isempty(SHR_ID)
       SHR_ID = @(shr) sprintf('shr%03d', shr);
    end

    % the data is layed out as follows:
    %
    % Data      Subject         FactorA FactorB
    % 0.10      T1              S1      SHARD1
    % 0.20      T2              S1      SHARD1
    % 0.30      T1              S2      SHARD1
    % 0.40      T2              S2      SHARD1
    % 0.50      T1              S3      SHARD1
    % 0.60      T2              S3      SHARD1
    % 0.15      T1              S1      SHARD2
    % 0.25      T2              S1      SHARD2
    % 0.35      T1              S2      SHARD2
    % 0.45      T2              S2      SHARD2
    % 0.55      T1              S3      SHARD2
    % 0.65      T2              S3      SHARD2

    data.tpc1 = NaN(1, N.tpc1);
    data.tpc2 = NaN(1, N.tpc2);
    shards = cell(1, S);
    
    % for each shard, 
    for shr = 1:S
        %copy the measure on that shard in the correct range of the data
        data.tpc1((shr-1)*T.tpc1*R + 1 : shr*T.tpc1*R) = measures{shr}{tpc1, :}(:);
        data.tpc2((shr-1)*T.tpc2*R + 1 : shr*T.tpc2*R) = measures{shr}{tpc2, :}(:);
        
        % add the identifier of the shard to the list of shards
        shards{shr} = SHR_ID(shr);
    end
    
    % grouping variable for the subjects (topic)
    subject.tpc1 = repmat(measures{1}.Properties.RowNames(tpc1), S*R, 1);
    subject.tpc2 = repmat(measures{1}.Properties.RowNames(tpc2), S*R, 1);

    % grouping variable for factorA (system)
    factorA.tpc1 = repmat(measures{1}.Properties.VariableNames, T.tpc1, 1);
    factorA.tpc1 = repmat(factorA.tpc1(:), S, 1);
    
    factorA.tpc2 = repmat(measures{1}.Properties.VariableNames, T.tpc2, 1);
    factorA.tpc2 = repmat(factorA.tpc2(:), S, 1);

    % grouping variable for factorB (shard)
    factorB.tpc1 = repmat(shards, T.tpc1*R, 1);
    factorB.tpc1 = factorB.tpc1(:);
    
    factorB.tpc2 = repmat(shards, T.tpc2*R, 1);
    factorB.tpc2 = factorB.tpc2(:);
end



