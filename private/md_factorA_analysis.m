
%% md_factorA_analysis
% 
% Analyses the system factor for the different ANOVA models.

%% Synopsis
%
%   [soa, me] = md_factorA_analysis(tag, ts, sts, N, T, R, S, data, factorA, soa, me)
%  
% *Parameters*
%
% * *|tag|* - the tag of the performed ANOVA analysis
% * *|ts|* - the topic set sample.
% * *|sts|* - the statistics about the performed ANOVA analysis for each
% topic set.
% * *|N|* - the total number of data for each topic set.
% * *|T|* - the total number of topics for each topic set.
% * *|R|* - the total number of runs.
% * *|S|* - the total number of shards. It is |NaN| is shards are not used.
% * *|data|* - the actual data used in the ANOVA analysis . 
% * *|factorA|* - the data labels used in the ANOVA analysis for factorA.
% * *|soa|* - the strength-of-association according to different parameters 
% for the system factor.
% * *|me|* - various information about the main effects for the system 
% factor.
%
% *Returns*
%
% * *|soa|* - the strength-of-association according to different parameters 
% for the system factor.
% * *|pwr|* - the power and its parameters for the system factor.
% * *|me|* - various information about the main effects for the system 
% factor.
%
% Each returned variable is a struct where fields |tpc1| and |tpc2|  keep
% the information about the first and the second topics set, respectively.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [soa, me] = md_factorA_analysis(tag, ts, sts, N, T, R, S, data, factorA, soa, me)

    % load common parameters
    common_parameters
    
    % compute the strength of association
    soa.omega2p.factorA.tpc1(ts) = sts.df_factorA.tpc1 * (sts.F_factorA.tpc1 - 1) / (sts.df_factorA.tpc1 * (sts.F_factorA.tpc1 - 1) + N.tpc1);
    
    % when not significant effects, it could be negative. Set it to NaN so
    % that it is omitted in averaging.
    if(soa.omega2p.factorA.tpc1(ts) < 0)
        soa.omega2p.factorA.tpc1(ts) = NaN;
    end    
    
    soa.omega2p.factorA.tpc2(ts) = sts.df_factorA.tpc2 * (sts.F_factorA.tpc2 - 1) / (sts.df_factorA.tpc2 * (sts.F_factorA.tpc2 - 1) + N.tpc2);
    
    % when not significant effects, it could be negative. Set it to NaN so
    % that it is omitted in averaging.
    if(soa.omega2p.factorA.tpc2(ts) < 0)
        soa.omega2p.factorA.tpc2(ts) = NaN;
    end

    % main effects
    [me.factorA.mean.tpc1(ts, :), me.factorA.std.tpc1(ts, :)] = grpstats(data.tpc1(:), factorA.tpc1(:), {'mean', 'std'});

    [me.factorA.mean.tpc2(ts, :), me.factorA.std.tpc2(ts, :)] = grpstats(data.tpc2(:), factorA.tpc2(:), {'mean', 'std'});

    % compute the Tukey CI
    if isnan(S) % for ANOVA models MD1 and  MD2
        me.factorA.tukey.q.tpc1(ts) = internal.stats.stdrinv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_error.tpc1, R);
        me.factorA.tukey.halfWidth.tpc1(ts) = me.factorA.tukey.q.tpc1(ts) * sqrt(sts.ms_error.tpc1/T.tpc1) / 2;
        
        me.factorA.tukey.q.tpc2(ts) = internal.stats.stdrinv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_error.tpc2, R);
        me.factorA.tukey.halfWidth.tpc2(ts) = me.factorA.tukey.q.tpc2(ts) * sqrt(sts.ms_error.tpc2/T.tpc2) / 2;
    else % for ANOVA model MD3
        me.factorA.tukey.q.tpc1(ts) = internal.stats.stdrinv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_error.tpc1, R);
        me.factorA.tukey.halfWidth.tpc1(ts) = me.factorA.tukey.q.tpc1(ts) * sqrt(sts.ms_error.tpc1/(T.tpc1*S)) / 2;
        
        me.factorA.tukey.q.tpc2(ts) = internal.stats.stdrinv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_error.tpc2, R);
        me.factorA.tukey.halfWidth.tpc2(ts) = me.factorA.tukey.q.tpc2(ts) * sqrt(sts.ms_error.tpc2/(T.tpc2*S)) / 2;
    end
    
    % when complex (or negative), set it to NaN so that it is excluded from
    % averages. This could happen if SS_error is zero or negative (due to
    % bad fit)
    if ( me.factorA.tukey.halfWidth.tpc1(ts) <= 0 || ~isreal(me.factorA.tukey.halfWidth.tpc1(ts)) )
        soa.omega2p.factorA.tpc1(ts) = NaN;
        me.factorA.tukey.halfWidth.tpc1(ts) = NaN;
    end
    
    % when complex (or negative), set it to NaN so that it is excluded from
    % averages. This could happen if SS_error is zero or negative (due to
    % bad fit)
    if ( me.factorA.tukey.halfWidth.tpc2(ts) <= 0 || ~isreal(me.factorA.tukey.halfWidth.tpc2(ts)) )
        soa.omega2p.factorA.tpc2(ts) = NaN;
        me.factorA.tukey.halfWidth.tpc2(ts) = NaN;
    end

    % compute the power
    if (sts.computePower)
        f2 = sts.ss_factorA.tpc1 / sts.ss_error.tpc1;
        Fc = finv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_factorA.tpc1, sts.df_error.tpc1);
        lambda = f2 * N.tpc1;        
        me.factorA.pwr.tpc1(ts) = ncfcdf(Fc, sts.df_factorA.tpc1, sts.df_error.tpc1, lambda, 'upper');

        f2 = sts.ss_factorA.tpc2 / sts.ss_error.tpc2;
        Fc = finv(1 - EXPERIMENT.analysis.alpha.threshold, sts.df_factorA.tpc2, sts.df_error.tpc2);
        lambda = f2 * N.tpc2;        
        me.factorA.pwr.tpc2(ts) = ncfcdf(Fc, sts.df_factorA.tpc2, sts.df_error.tpc2, lambda, 'upper');
    end


    % multiple comparison of systems
    me.factorA.tukey.mc.tpc1{ts} = EXPERIMENT.analysis.(tag).multcompare(sts.tpc1);
    me.factorA.tukey.mc.tpc2{ts} = EXPERIMENT.analysis.(tag).multcompare(sts.tpc2);
end

