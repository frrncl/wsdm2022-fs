
%% md_data_structures
% 
% Allocates the data structures common to all the ANOVA analyses.

%% Synopsis
%
%   [tbl, soa, me, cmp] = md_data_structures(runIdentifiers,  R, T, MD)
%  
% *Parameters*
%
% * *|runIdentifiers|* - the identifiers of the analysed runs.
% * *|R|* - the total number of analysed runs.
% * *|T|* - the total number of topics in each topic set.
% * *|MD|* - the ANOVA model applied to each topic set.
%
% *Returns*
%
% * *|tbl|* - the ANOVA tables.
% * *|soa|* - the strength-of-association according to different parameters 
% for the system factor.
% * *|me|* - various information about the main effects for the system 
% factor.
% * *|cmp|* - various information and statistics to compare the two
% topic sets
%
% Each returned variable is a struct where fields |tpc1| and |tpc2|  keep
% the information about the first and the second topics set, respectively.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [tbl, soa, me, cmp] = md_data_structures(runIdentifiers, R, T, MD)

    % load common parameters
    common_parameters
    
    %assert(length(runIdentifiers) == R, 'Number of run identifiers (%d) different from the expected number of runs (%d)', length(runIdentifiers), R);
    
    % preallocate data structures
    % each data structures is a struct where the fields |tpc1| and |tpc2|
    % keep the information about the first and the second topics set,
    % respectively.
    
    % the ANOVA tables
    tbl.tpc1 = cell(EXPERIMENT.analysis.topicSampling.samples, 1);
    tbl.tpc2 = cell(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % the strength-of-association according to different parameters for the
    % system factor
    soa.omega2p.factorA.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    soa.omega2p.factorA.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);

    % me contains various information about the main effects for the system 
    % factor
    
    % run idenfiers
    me.factorA.label = runIdentifiers;

    % the total number of possible system pairs
    me.factorA.pairs = R*(R - 1)/2;
    
    % marginal mean and standard deviation
    me.factorA.mean.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, R);
    me.factorA.std.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, R);
    
    me.factorA.mean.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, R);
    me.factorA.std.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, R);
    
    % Tukey confidence intervals, multiple comparisons, and top group
    me.factorA.tukey.q.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    me.factorA.tukey.halfWidth.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    me.factorA.tukey.mc.tpc1 = cell(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    me.factorA.tukey.q.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    me.factorA.tukey.halfWidth.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    me.factorA.tukey.mc.tpc2 = cell(EXPERIMENT.analysis.topicSampling.samples, 1);

    % the power associated to the system factor
    me.factorA.pwr.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    me.factorA.pwr.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % cmp contains various information and statistics to compare the two
    % topic sets
    
    % the total number of systems
    cmp.factorA.info.runs = R;
    
    % the total number of topics
    cmp.factorA.info.topics = T.size;
        
    % the total number of possible system pairs
    cmp.factorA.info.pairs = R*(R - 1)/2;
        
    % the total number of topics in topic set 1
    cmp.factorA.info.tpc1.size = T.tpc1;
    
    % the total number of topics in topic set 2
    cmp.factorA.info.tpc2.size = T.tpc2;
    
    % the ANOVA model applied to topic set 1
    cmp.factorA.info.tpc1.md = MD.tpc1;
    
    % the ANOVA model applied to topic set 2
    cmp.factorA.info.tpc2.md = MD.tpc2;
    
    % the used significance level alpha
    cmp.factorA.info.alpha = EXPERIMENT.analysis.alpha.threshold;
    
    % the correlation between the rankings of systems in the two topic
    % sets
    cmp.factorA.data.tau = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);

    % the effect size in the two topic sets
    cmp.factorA.data.soa.omega2p.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.soa.omega2p.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);

    % the power associated to the system factor
    cmp.factorA.data.pwr.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pwr.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
        
    % the Tukey confidence interval in the two topic sets
    cmp.factorA.data.tukey.halfWidth.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.tukey.halfWidth.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % the size of the significantly different systems set in the two topic 
    % sets
    cmp.factorA.data.pairs.sig.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pairs.sig.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);    
        
    
    % count how many times system S1 is significantly better than system S2
    cmp.factorA.data.pairs.s1SBs2.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pairs.s1SBs2.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % count how many times system S2 is significantly better than system S1
    cmp.factorA.data.pairs.s2SBs1.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pairs.s2SBs1.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % count how many times system S1 is not significantly better than system S2
    cmp.factorA.data.pairs.s1NBs2.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pairs.s1NBs2.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % count how many times system S2 is not significantly better than system S1
    cmp.factorA.data.pairs.s2NBs1.tpc1 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    cmp.factorA.data.pairs.s2NBs1.tpc2 = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    
    % Jaccard coefficient, i.e. |A \cap B|/|A \cup B|
    % See [Jaccard, 1901].
    cmp.factorA.data.pairs.jaccard = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Overlap coefficient, also called Szymkiewicz–Simpson coefficient,
    % i.e. |A \cap B|/min(|A|, |B|)
    % See [Szymkiewicz, 1934].
    cmp.factorA.data.pairs.overlap = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Active Agreements: on both topic sets tpc1 and tpc2, either S1 >> S2 
    % or S2 >> S1
    % The bigger, the better
    % It corresponds to
    % - SSA of [Moffat et al., ADCS 2012]
    % - Success of [Urbano et al., SIGIR 2013]
    % - Active Agreements of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.aa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Active Disagreements: S1 >> S2 on topic set tpc1 but S2 >> S1 on topic
    % set tpc2; or S2 >> S1 on topic set tpc1 but S1 >> S2 on topic set tpc2
    % The smaller, the better
    % It corresponds to
    % - SSD of [Moffat et al., ADCS 2012]
    % - Major Error of [Urbano et al., SIGIR 2013]
    % - Active Disagreements of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.ad = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
            
    % Passive Agreements: on both topic set tpc1 and tpc2, S1 > S2 or S2 > S1
    % The bigger, the better, since there is agreement and not everything 
    % can be significantly different; however, Active Agreements are better.
    % It corresponds to
    % - NN of [Moffat et al., ADCS 2012]
    % - (somehow) Non-significance of [Urbano et al., SIGIR 2013]
    % - Passive Agreements of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.pa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Passive Disagreements:  S1 > S2 on topic set tpc1 but S2 > S1 on topic
    % set tpc2; or S2 > S1 on topic set tpc1 but S1 > S2 on topic set tpc2
    % The smaller, the better
    % - (somehow) NN of [Moffat et al., ADCS 2012]
    % - (somehow) Non-significance of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.pd = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);

    % Mixed Agreements: S1 >> S2 on topic set tpc1 but S1 > S2 on topic
    % set tpc2; or S2 >> S1 on topic set tpc1 but S2 > S1 on topic set tpc2;
    % S1 > S2 on topic set tpc1 but S1 >> S2 on topic set tpc2; or S2 > S1 
    % on topic set tpc1 but S2 >> S1 on topic set tpc2;
    % The smaller, the better
    % It corresponds to
    % - SN + NS of [Moffat et al., ADCS 2012]
    % - Lack of Power of [Urbano et al., SIGIR 2013]
    % - Passive Disagreements of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.ma = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Mixed Disagreements: S1 >> S2 on topic set tpc1 but S2 > S1 on topic
    % set tpc2; or S2 >> S1 on topic set tpc1 but S1 > S2 on topic
    % set tpc2; or S1 > S2 on topic set tpc1 but S2 >> S1 on topic set tpc2;
    % or S2 > S1 on topic set tpc1 but S1 >> S2 on topic set tpc2;
    % The smaller, the better
    % It corresponds to
    % - Minor Error of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.md = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Rate of Active Agreements
    % It corresponds to
    % - Ratio1 (category SSA) of [Moffat et al., ADCS 2012]
    % - Success Rate of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.raa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Rate of Active Disagrements
    % It corresponds to
    % - Ratio2 (category SSD) of [Moffat et al., ADCS 2012]
    % - Major Error Rate of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.rad = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
        
    % Rate of Passive Agreements
    cmp.factorA.data.pairs.rpa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Rate of Passive Disagrements
    cmp.factorA.data.pairs.rpd = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Rate of Mixed Agreements
    % - Lack of Power Rate of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.rma = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Rate of Mixed Disagrements
    % It corresponds to
    % - Minor Error Rate of [Urbano et al., SIGIR 2013]
    cmp.factorA.data.pairs.rmd = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Proportion of Active Agreements, i.e. the percentage agreement when 
    % both topic sets report significant differences. It represents how many 
    % times approaches agree on two systems being significantly different 
    % on both topic sets with respect to the total number of times two 
    % systems have been found to be significantly different. The bigger 
    % this ratio, the more stable is  the decision about which systems are 
    % significantly different, independently from the topic sets. 
    % It corresponds to
    % - Ratio3 [2SSA/(2SSA + SN + NS)] of [Moffat et al., ADCS 2012]
    % - Proportion of Active Agreements [2AA/(2AA + PD)] of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.paa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
    
    % Proportion of Passive Agreements, i.e. the percentage agreement when 
    % both topic sets report no significant differences. It represents how  
    % many times approaches agree on two systems being not significantly  
    % different on both topic sets with respect to  the total number of  
    % times two systems have been found to be not significantly different. 
    % The bigger this ratio, the more stable is  the decision about which 
    % systems are  not significantly different, independently from the 
    % topic sets. 
    % It corresponds to
    % - Ratio4 [2NN/(2NN + SN + NS)] of [Moffat et al., ADCS 2012]
    % - Proportion of Passive Agreements [2PA/(2PA + PD)] of [Faggioli and Ferro, 2020]
    cmp.factorA.data.pairs.ppa = NaN(EXPERIMENT.analysis.topicSampling.samples, 1);
            
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.soa.omega2p.tpc1|
    cmp.factorA.sts.soa.omega2p.tpc1.mean = NaN;
    cmp.factorA.sts.soa.omega2p.tpc1.std = NaN;
    cmp.factorA.sts.soa.omega2p.tpc1.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.soa.omega2p.tpc2|
    cmp.factorA.sts.soa.omega2p.tpc2.mean = NaN;
    cmp.factorA.sts.soa.omega2p.tpc2.std = NaN;
    cmp.factorA.sts.soa.omega2p.tpc2.ci = NaN;
    
    % the p-value of an unpaired t-test between |cmp.factorA.data.soa.omega2p.tpc1| 
    % and |cmp.factorA.data.soa.omega2p.tpc1| to tell whether the effect size in
    % the two topic sets is significantly different
    cmp.factorA.sts.soa.omega2p.p = NaN;
    

    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pwr.tpc1|
    cmp.factorA.sts.pwr.tpc1.mean = NaN;
    cmp.factorA.sts.pwr.tpc1.std = NaN;
    cmp.factorA.sts.pwr.tpc1.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pwr.tpc2|
    cmp.factorA.sts.pwr.tpc2.mean = NaN;
    cmp.factorA.sts.pwr.tpc2.std = NaN;
    cmp.factorA.sts.pwr.tpc2.ci = NaN;
    
    % the p-value of an unpaired t-test between |cmp.factorA.data.pwr.tpc1| 
    % and |cmp.factorA.data.pwr.tpc2| to tell whether the effect size in
    % the two topic sets is significantly different
    cmp.factorA.sts.pwr.p = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tukey.halfWidth.tpc1|
    cmp.factorA.sts.tukey.halfWidth.tpc1.mean = NaN;
    cmp.factorA.sts.tukey.halfWidth.tpc1.std = NaN;
    cmp.factorA.sts.tukey.halfWidth.tpc1.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tukey.halfWidth.tpc2|
    cmp.factorA.sts.tukey.halfWidth.tpc2.mean = NaN;
    cmp.factorA.sts.tukey.halfWidth.tpc2.std = NaN;
    cmp.factorA.sts.tukey.halfWidth.tpc2.ci = NaN;
        
    % the p-value of an unpaired t-test between 
    % |cmp.factorA.data.tukey.halfWidth.tpc1| and 
    % |cmp.factorA.data.tukey.halfWidth.tpc2| to tell whether the Tukey
    % confidence interval in the two topic sets is significantly different
    cmp.factorA.sts.tukey.halfWidth.p = NaN;
    
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.tau|
    cmp.factorA.sts.tau.mean = NaN;
    cmp.factorA.sts.tau.std = NaN;
    cmp.factorA.sts.tau.ci = NaN;           
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.sig.tpc1|
    cmp.factorA.sts.pairs.sig.tpc1.mean = NaN;
    cmp.factorA.sts.pairs.sig.tpc1.std = NaN;
    cmp.factorA.sts.pairs.sig.tpc1.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.sig.tpc2|
    cmp.factorA.sts.pairs.sig.tpc2.mean = NaN;
    cmp.factorA.sts.pairs.sig.tpc2.std = NaN;
    cmp.factorA.sts.pairs.sig.tpc2.ci = NaN;
    
    % the p-value of an unpaired t-test between 
    % |cmp.factorA.data.pairs.sig.tpc1| and |cmp.factorA.data.pairs.sig.tpc2| 
    % to tell whether the size of the significantly different systems sets 
    % in the two topic sets is significantly different
    cmp.factorA.sts.pairs.sig.p = NaN;
        
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.jaccard|
    cmp.factorA.sts.pairs.jaccard.mean = NaN;
    cmp.factorA.sts.pairs.jaccard.std = NaN;
    cmp.factorA.sts.pairs.jaccard.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.overlap|
    cmp.factorA.sts.pairs.overlap.mean = NaN;
    cmp.factorA.sts.pairs.overlap.std = NaN;
    cmp.factorA.sts.pairs.overlap.ci = NaN;
    
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.aa|
    cmp.factorA.sts.pairs.aa.mean = NaN;
    cmp.factorA.sts.pairs.aa.std = NaN;
    cmp.factorA.sts.pairs.aa.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ad|
    cmp.factorA.sts.pairs.ad.mean = NaN;
    cmp.factorA.sts.pairs.ad.std = NaN;
    cmp.factorA.sts.pairs.ad.ci = NaN;
    
     % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.pa|
    cmp.factorA.sts.pairs.pa.mean = NaN;
    cmp.factorA.sts.pairs.pa.std = NaN;
    cmp.factorA.sts.pairs.pa.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.pd|
    cmp.factorA.sts.pairs.pd.mean = NaN;
    cmp.factorA.sts.pairs.pd.std = NaN;
    cmp.factorA.sts.pairs.pd.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ma|
    cmp.factorA.sts.pairs.ma.mean = NaN;
    cmp.factorA.sts.pairs.ma.std = NaN;
    cmp.factorA.sts.pairs.ma.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.md|
    cmp.factorA.sts.pairs.md.mean = NaN;
    cmp.factorA.sts.pairs.md.std = NaN;
    cmp.factorA.sts.pairs.md.ci = NaN;
    
    
        
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.raa|
    cmp.factorA.sts.pairs.raa.mean = NaN;
    cmp.factorA.sts.pairs.raa.std = NaN;
    cmp.factorA.sts.pairs.raa.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rad|
    cmp.factorA.sts.pairs.rad.mean = NaN;
    cmp.factorA.sts.pairs.rad.std = NaN;
    cmp.factorA.sts.pairs.rad.ci = NaN;
    
     % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rpa|
    cmp.factorA.sts.pairs.rpa.mean = NaN;
    cmp.factorA.sts.pairs.rpa.std = NaN;
    cmp.factorA.sts.pairs.rpa.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rpd|
    cmp.factorA.sts.pairs.rpd.mean = NaN;
    cmp.factorA.sts.pairs.rpd.std = NaN;
    cmp.factorA.sts.pairs.rpd.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rma|
    cmp.factorA.sts.pairs.rma.mean = NaN;
    cmp.factorA.sts.pairs.rma.std = NaN;
    cmp.factorA.sts.pairs.rma.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.rmd|
    cmp.factorA.sts.pairs.rmd.mean = NaN;
    cmp.factorA.sts.pairs.rmd.std = NaN;
    cmp.factorA.sts.pairs.rmd.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.paa|
    cmp.factorA.sts.pairs.paa.mean = NaN;
    cmp.factorA.sts.pairs.paa.std = NaN;
    cmp.factorA.sts.pairs.paa.ci = NaN;
    
    % the mean, standard deviation, and confidence interval of 
    % |cmp.factorA.data.pairs.ppa|
    cmp.factorA.sts.pairs.ppa.mean = NaN;
    cmp.factorA.sts.pairs.ppa.std = NaN;
    cmp.factorA.sts.pairs.ppa.ci = NaN;
    
end

