%% print_mdf_analysis_report_csv
% 
% Reports a summary about the Fake ANOVA analyses and saves them to a |.csv|
% file.
%
%% Synopsis
%
%   [] = print_mdf_analysis_report_csv(trackID, shardIDs, tsIDs, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|shardIDs|* - a cell array with the identifier of the shards to be used.
% * *|tsID|* - a cell array with the identifiers of the topic sampling process to apply.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_mdf_analysis_report_csv(trackID, shardIDs, tsIDs, startMeasure, endMeasure)

    persistent  MDF12H0 MDF12H1 MDF3H0 MDF3H1 SHARD_SAMPLE;

    if isempty(MDF12H0)
        MDF12H0 = 'mdf12h0';
        MDF12H1 = 'mdf12h1';
        MDF3H0 = 'mdf3h0';
        MDF3H1 = 'mdf3h1';        
        SHARD_SAMPLE = 1;
    end

    % check the number of input parameters
    narginchk(3, 5);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    
    for s = 1:length(shardIDs)
        
        shardID = shardIDs{s};
        
        % check that shardID is a non-empty cell array
        validateattributes(shardID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');
        
        if iscell(shardID)
            % check that shardID is a cell array of strings with one element
            assert(iscellstr(shardID) && numel(shardID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        shardID = char(strtrim(shardID));
        shardID = shardID(:).';
        
        % check that shardID assumes a valid value
        validatestring(shardID, ...
            EXPERIMENT.shard.list, '', 'shardID');
        
        % check that the track and the shard rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.shard.(shardID).corpus), 'Track %s and shard %s do not rely on the same corpus', trackID, shardID);
        
    end % for splitIDs
    
    for s = 1:length(tsIDs)
        
        tsID = tsIDs{s};
        
        % check that tsID is a non-empty cell array
        validateattributes(tsID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'tsID');
        
        if iscell(tsID)
            % check that tsID is a cell array of strings with one element
            assert(iscellstr(tsID) && numel(tsID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        tsID = char(strtrim(tsID));
        tsID = tsID(:).';
        
        % check that tsID assumes a valid value
        validatestring(tsID, ...
            EXPERIMENT.analysis.topicSampling.list, '', 'tsID');
        
    end % for tsIDs

    if nargin == 5
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;


    fprintf('\n\n######## Reporting summary Fake ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - track: %s\n', trackID);
    
    fprintf('  - shards:\n');
    for s = 1:length(shardIDs)
        
        shardID = shardIDs{s};        
        
        fprintf('    * shard: %s - %d shards\n', shardID, EXPERIMENT.shard.(shardID).shard);
    end
    
    fprintf('  - topic sampling:\n');
    for s = 1:length(tsIDs)
        
        tsID = tsIDs{s};        
        
        fprintf('    * topic sampling: %s - %s (%d%% - %d%%)\n', ...
            EXPERIMENT.analysis.topicSampling.(tsID).label,  ...
            EXPERIMENT.analysis.topicSampling.(tsID).description, ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc1 * 100), ...
            round(EXPERIMENT.analysis.topicSampling.(tsID).tpc2 * 100));
    end
    
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

        
    fprintf('+ Printing the report\n');
    fprintf('  ');

    data = [];
         
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
                 
        tsIDNum = length(tsIDs);
                
        %##### FAKE MODELS SIGNIFICANT
        
        for i = 1:tsIDNum
            tsID = tsIDs{i};
            
            try
                
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF12H1, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF12H1, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                    'WorkspaceVarNames', {'cmp'}, ...
                    'FileVarNames', {anovaCmpID});
                
                tmp = {EXPERIMENT.measure.getAcronym(m), ...
                    upper(MDF12H1), ...
                    EXPERIMENT.analysis.topicSampling.(tsID).label, ...
                    cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size, ...
                    cmp.factorA.sts.tau.mean, ...
                    cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc2.mean, ...
                    cmp.factorA.sts.soa.omega2p.p, ...
                    cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.mean, ...
                    cmp.factorA.sts.tukey.halfWidth.p, ...
                    cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc2.mean, ...
                    cmp.factorA.sts.pairs.sig.p, ...
                    cmp.factorA.sts.pairs.jaccard.mean, ...
                    cmp.factorA.sts.pairs.overlap.mean, ...
                    cmp.factorA.sts.pairs.aa.mean,  ...
                    cmp.factorA.sts.pairs.ad.mean,  ...
                    cmp.factorA.sts.pairs.pa.mean,  ...
                    cmp.factorA.sts.pairs.pd.mean,  ...
                    cmp.factorA.sts.pairs.ma.mean,  ...
                    cmp.factorA.sts.pairs.md.mean,  ...
                    cmp.factorA.sts.pairs.paa.mean,  ...
                    cmp.factorA.sts.pairs.ppa.mean, ...
                    cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean)};
                
                data = [data; tmp];
                
            catch
                % do nothing
            end
            
            
            clear cmp;
            
        end % for topic set sample
        
        
        %load MD3 data
        for j = 1:length(shardIDs)
            shardID = shardIDs{j};
            
            for i = 1:tsIDNum
                tsID = tsIDs{i};
                
                try
                    
                    anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MDF3H1, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
                    anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(MDF3H1, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, SHARD_SAMPLE), tsID, trackID);
                                                            
                    serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
                        'WorkspaceVarNames', {'cmp'}, ...
                        'FileVarNames', {anovaCmpID});
                                                            
                    tmp = {EXPERIMENT.measure.getAcronym(m), ...
                           [upper(MDF3H1) ' - ', shardID], ...
                           EXPERIMENT.analysis.topicSampling.(tsID).label, ...
                           cmp.factorA.info.tpc1.size,  cmp.factorA.info.tpc2.size, ...
                           cmp.factorA.sts.tau.mean, ...
                           cmp.factorA.sts.soa.omega2p.tpc1.mean, cmp.factorA.sts.soa.omega2p.tpc2.mean, ...
                           cmp.factorA.sts.soa.omega2p.p, ...
                           cmp.factorA.sts.tukey.halfWidth.tpc1.mean, cmp.factorA.sts.tukey.halfWidth.tpc2.mean, ...
                           cmp.factorA.sts.tukey.halfWidth.p, ...
                           cmp.factorA.sts.pairs.sig.tpc1.mean, cmp.factorA.sts.pairs.sig.tpc2.mean, ...
                           cmp.factorA.sts.pairs.sig.p, ...
                           cmp.factorA.sts.pairs.jaccard.mean, ...
                           cmp.factorA.sts.pairs.overlap.mean, ...
                           cmp.factorA.sts.pairs.aa.mean,  ...
                           cmp.factorA.sts.pairs.ad.mean,  ...
                           cmp.factorA.sts.pairs.pa.mean,  ...
                           cmp.factorA.sts.pairs.pd.mean,  ...
                           cmp.factorA.sts.pairs.ma.mean,  ...
                           cmp.factorA.sts.pairs.md.mean,  ...
                           cmp.factorA.sts.pairs.paa.mean,  ...
                           cmp.factorA.sts.pairs.ppa.mean, ...
                           cmp.factorA.sts.pairs.aa.mean / (cmp.factorA.sts.pairs.aa.mean + cmp.factorA.sts.pairs.ad.mean + cmp.factorA.sts.pairs.ma.mean + cmp.factorA.sts.pairs.md.mean)};
                                                            
                    data = [data; tmp];
                                                        
                catch
                    % do nothing                    
                end
                                
                clear cmp;
            end % for topic set sample
                        
        end % for shard
        
    end % for measure
    
    out = cell2table(data, 'VariableNames', ... 
         {'Measure', 'Model', 'Topic_Set', 'TS1_Size', 'TS2_Size', ...
            'tau', 'omega2_TS1', 'omega2_TS2', 'omega2_p', ...
            'Tukey_TS1', 'Tukey_TS2', 'Tukey_p', ...
            'Pairs_TS1', 'Pairs_TS2', 'Pairs_p', 'Jaccard', 'Overlap', ...
            'AA', 'AD', 'PA', 'PD', 'MA', 'MD', 'PAA', 'PPA', 'NEW'});         
     
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.report.mdfanalysis(trackID);
    
    writetable(out, EXPERIMENT.pattern.file.report(trackID, reportID, 'csv'), ...
                'FileType', 'text', 'WriteVariableNames', true, 'Delimiter', 'comma')
    
                           
    fprintf('\n\n######## Total elapsed time for reporting summary Fake ANOVA analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
