%% sample_topics
% 
% Sample topics into two sets.
%
%% Synopsis
%
%   [] = sample_topics(trackID, tsID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|tsID|* - the identifier of the topic sampling process to apply.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = sample_topics(trackID, tsID)
    
    % check that we have the correct number of input arguments. 
    narginchk(2, 2);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that tsID is a non-empty string
    validateattributes(tsID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'smplID');

    if iscell(tsID)
        % check that tsID is a cell array of strings with one element
        assert(iscellstr(tsID) && numel(tsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tsID = char(strtrim(tsID));
    tsID = tsID(:).';
    
    % check that smplID assumes a valid value
    validatestring(tsID, ...
        EXPERIMENT.analysis.topicSampling.list, '', 'tsID');  
    
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Sampling topics for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - topic sampling approach: %s\n', tsID);

    
     fprintf('+ Sampling topics\n');
    
    % total number of topics in the track
    T = EXPERIMENT.track.(trackID).topics;
    
    % compute the number of topics in each topic set
    ts1 = ceil(T*EXPERIMENT.analysis.topicSampling.(tsID).tpc1);
    ts2 = ceil(T*EXPERIMENT.analysis.topicSampling.(tsID).tpc2);
    
    % check for approximation errors and ajdust, if needed
    if (ts1 + ts2 > T)
        ts2  = ts2 - (ts1 + ts2 - T);
    end
    
    % generate the samples with/without replacement
    switch(EXPERIMENT.analysis.topicSampling.(tsID).sampling)
        case 'prt'
            
            samples = NaN(EXPERIMENT.analysis.topicSampling.samples, T);
            
            for s = 1:EXPERIMENT.analysis.topicSampling.samples
                samples(s, :) = randperm(T);
            end
                                    
        case 'rpl'
            
            samples = randi(T, EXPERIMENT.analysis.topicSampling.samples, T);
            
    end
    
    % sample topics for the first topic set
    tpc1 = samples(:, 1:ts1);    
    tpc1ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(1, tsID, trackID);
            
    % sample topics for the second topic set
    tpc2 = samples(:, (1:ts2) + ts1);
    tpc2ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(2, tsID, trackID);
        
    sersave2(EXPERIMENT.pattern.file.dataset.topic(trackID, EXPERIMENT.pattern.identifier.topicSampling.tsetID(tsID, trackID)), ...
        'WorkspaceVarNames', {'tpc1', 'tpc2'}, ...
        'FileVarNames', {tpc1ID, tpc2ID});
    
    
    fprintf('\n\n######## Total elapsed time for sampling topics for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end

