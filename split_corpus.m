%% split_corpus
% 
% Splits a corpus into a set of random shards.
%
%% Synopsis
%
%   [] = split_corpus(shardID)
%  
%
% *Parameters*
%
% * *|shardID|* - the identifier of the shards to be used.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_corpus(shardID)
   
    % check that we have the correct number of input arguments. 
    narginchk(1, 1);
    
    % setup common parameters
    common_parameters;
              
    % check that shardID is a non-empty string
    validateattributes(shardID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');

    if iscell(shardID)
        % check that shardID is a cell array of strings with one element
        assert(iscellstr(shardID) && numel(shardID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    shardID = char(strtrim(shardID));
    shardID = shardID(:).';    
    
     % check that shardID assumes a valid value
    validatestring(shardID, ...
        EXPERIMENT.shard.list, '', 'shardID');  
            
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting corpus %s into shards (%s) ########\n\n', ...
        EXPERIMENT.shard.(shardID).corpus, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - corpus: %s\n', EXPERIMENT.shard.(shardID).corpus);
    fprintf('  - shard: %s\n', shardID);
    fprintf('    + number of shards: %d\n', EXPERIMENT.shard.(shardID).shard);
    fprintf('    + shard ratio(s): %s\n', num2str(EXPERIMENT.shard.(shardID).ratio, '%3.2f '));
    fprintf('    + number of samples: %d\n\n', EXPERIMENT.shard.sample);
    
    fprintf('+ Loading the corpus\n');
    
    corpus = importdata(EXPERIMENT.pattern.file.corpus(EXPERIMENT.shard.(shardID).corpus));
    
    % the total lenght of the corpus
    N = length(corpus);
    
    assert(N == EXPERIMENT.corpus.(EXPERIMENT.shard.(shardID).corpus).size, 'Expected corpus size %d; actual corpus size %d', ...
        EXPERIMENT.corpus.(EXPERIMENT.shard.(shardID).corpus).size, N);
        
    fprintf('+ Computing the shards\n');
    
    % turn the shard ratios into number of documents for each shard
    range = floor(EXPERIMENT.shard.(shardID).ratio * N);
    
    % find the position of the biggest element
    [~, idx] = max(range);
    
    % get only the first biggest element in case of ties
    idx = idx(1);
    
    % add to the biggest element any round up, i.e. 
    range(idx) = range(idx) + (N - sum(range));
    
    % ensure it is a row
    range = range(:).';
    
    % turn the number of elements in each split into the last index of the
    % range of each split
    range = cumsum(range);
    
    % add the start index of the range of each split (first row)
    range = [1 range(1:end-1)+1; range];
    
    fprintf('+ Generating the shards for the requested samples\n');
    
    % repeat the shard generation for each sample
    for smpl = 1:EXPERIMENT.shard.sample
        
        fprintf('  - sample %d\n', smpl);
    
        % generate a random sampling of the corpus
        corpus = corpus(randperm(N));
        
        for shr = 1:length(EXPERIMENT.shard.(shardID).ratio)
            
            shrID = EXPERIMENT.pattern.identifier.shard.full(shardID, shr, smpl);
            
            fprintf('    * shard %s\n', shrID);
            
            fid = fopen(EXPERIMENT.pattern.file.shard(shardID, shrID), 'w');
            
            tmp = corpus(range(1, shr):range(2, shr));
            
            fprintf(fid,'%s\n', tmp{:});
            
            fclose(fid);
            
        end % for each shard    
    end % for each sample
       
    fprintf('\n\n######## Total elapsed time for splitting corpus %s into shards (%s): %s ########\n\n', ...
            EXPERIMENT.shard.(shardID).corpus, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));    
end
