%% compute_cmd1to2_analysis
% 
% Compares the MD1 and MD2 behaviour on a topic set wrt to MD3.
%
%% Synopsis
%
%   [] = compute_cmd1to2_analysis(trackID, tsID, splitID, startMeasure, endMeasure, startSample, endSample, threads)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|shardID|* - the identifier of the shards to be used.
% * *|tsID|* - the identifier of the topic sampling process to apply.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
% * *|startSample|* - the index of the start shard sample to process. Optional.
% * *|endSample|* - the index of the end shard sample to process. Optional.
% * *|threads|* - the maximum number of threads to be used. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = compute_cmd1to2_analysis(trackID, shardID, tsID, startMeasure, endMeasure, startSample, endSample, threads)

    persistent TAG MD1 MD2 MD3;
    
    if isempty(TAG)
        MD1 = 'md1';
        MD2 = 'md2';
        MD3 = 'md3';
        TAG = {MD1, MD2};
    end

    % check the number of input parameters
    narginchk(3, 8);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
                 
    
    % check that shardID is a non-empty cell array
    validateattributes(shardID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');
    
    if iscell(shardID)
        % check that shardID is a cell array of strings with one element
        assert(iscellstr(shardID) && numel(shardID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    shardID = char(strtrim(shardID));
    shardID = shardID(:).';
    
    % check that splitID assumes a valid value
    validatestring(shardID, ...
        EXPERIMENT.shard.list, '', 'shardID');
    
    % check that the track and the shard rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.shard.(shardID).corpus), 'Track %s and shard %s do not rely on the same corpus', trackID, shardID);
    
    
    % check that tsID is a non-empty string
    validateattributes(tsID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'smplID');

    if iscell(tsID)
        % check that tsID is a cell array of strings with one element
        assert(iscellstr(tsID) && numel(tsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tsID = char(strtrim(tsID));
    tsID = tsID(:).';
    
    % check that tsID assumes a valid value
    validatestring(tsID, ...
        EXPERIMENT.analysis.topicSampling.list, '', 'tsID');  
                
        
    if nargin >= 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    if nargin >= 6
        validateattributes(startSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.shard.sample }, '', 'startSample');
        
        validateattributes(endSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startSample, '<=', EXPERIMENT.shard.sample }, '', 'endSample');
    else 
        startSample = 1;
        endSample = EXPERIMENT.shard.sample;
    end
    
    if nargin == 8
        % the number of threads must be at maximum equal to the number of
        % physical cores
        validateattributes(threads, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', feature('numcores') }, '', 'threads');
        
        maxNumCompThreads(threads);
    else
        threads = maxNumCompThreads('automatic');        
    end
    
    % start of overall computations
    startComputation = tic;
          
                  
    fprintf('\n\n######## Comparing md1 and md2 wrt md3 ANOVA analyses on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * topic sampling: %s\n', tsID);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - shard: %s\n', shardID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.shard.(shardID).shard);
    fprintf('    * start sample: %d\n', startSample);
    fprintf('    * end sample: %d\n', endSample);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - threads: %d\n\n', threads);
                  
    
    fprintf('\n+ Loading topic samples \n');
    
    tpc1ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(1, tsID, trackID);
    tpc2ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(2, tsID, trackID);
    
    serload2(EXPERIMENT.pattern.file.dataset.topic(trackID, EXPERIMENT.pattern.identifier.topicSampling.tsetID(tsID, trackID)), ...
        'WorkspaceVarNames', {'tpc1', 'tpc2'}, ...
        'FileVarNames', {tpc1ID, tpc2ID});
    
    % the number of topics and runs
    
    R = EXPERIMENT.track.(trackID).runs;
    
    % MD1/2 is always mapped to topic set tpc1 while MD3 is always mapped
    % to topic set tpc2
    %
    % MD1/2 is always assigned the largest topic set
    if size(tpc1, 2) >=  size(tpc2, 2)
        T.tpc1 = size(tpc1, 2);
        T.tpc2 = size(tpc2, 2);
        T.ts.tpc1 = 'tpc1';
        T.ts.tpc2 = 'tpc2';
    else
        T.tpc1 = size(tpc2, 2);
        T.tpc2 = size(tpc1, 2);
        T.ts.tpc1 = 'tpc2';
        T.ts.tpc2 = 'tpc1';
    end
    T.size = EXPERIMENT.track.(trackID).topics;
        
    % the ANOVA model applied to the second topic set
    MD.tpc2 = MD3;
    
    % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        % repeat the computation for each sample of shards
        for smpl = startSample:endSample
            
            fprintf('  - shard sample %d\n', smpl);
                        
            % for each analysis type
            for md = 1:length(TAG)
                
                % the ANOVA model applied to the  first topic set
                MD.tpc1 = TAG{md};
                
                % preallocate data structures
                [~, soa, me, cmp] = md_data_structures({}, R, T, MD);
                
                
                % perform analyses on each topic set sample
                for ts = 1:EXPERIMENT.analysis.topicSampling.samples
                    
                    
                    fprintf('    * topic set sample %d\n', ts);
                    
                    fprintf('      # loading the %s ANOVA analysis\n', TAG{md});
                    
                    % load MD{1, 2} data
                    anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                    anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                    anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG{md}, mid, EXPERIMENT.track.(trackID).corpus, tsID, trackID);
                                        
                    serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                        'WorkspaceVarNames', {'md12_me', 'md12_soa'}, ...
                        'FileVarNames', {anovaMeID, anovaSoAID});
                                        
                    fprintf('      # loading the %s ANOVA analysis\n', MD3);
                    
                    % load MD3 data
                    anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD3, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                    anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD3, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                    anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD3, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);

                    serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
                        'WorkspaceVarNames', {'md3_me', 'md3_soa'}, ...
                        'FileVarNames', {anovaMeID, anovaSoAID});
                                        
                    % compare the two topic sets on the above analyses
                    [me, soa, cmp] = cmd_factorA_compare_ts(T.ts.tpc1, T.ts.tpc2, ts, md12_me, md12_soa, md3_me, md3_soa, me, soa, cmp);
                                        
                    clear md12_me md12_soa md3_me md3_soa;
                    
                end  % for topic samples
                
                
                % compute summary statistics about the comparison between the
                % two topic sets;
                [cmp] = md_factorA_summary_ts(cmp);
                
                
                fprintf('      # saving the analyses\n');
                
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(EXPERIMENT.pattern.identifier.cmd(TAG{md}, true), mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaMeID = EXPERIMENT.pattern.identifier.anova.me(EXPERIMENT.pattern.identifier.cmd(TAG{md}, true), mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(EXPERIMENT.pattern.identifier.cmd(TAG{md}, true), mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(EXPERIMENT.pattern.identifier.cmd(TAG{md},  true), mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                
                sersave2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
                    'WorkspaceVarNames', {'me',  'soa', 'cmp'}, ...
                    'FileVarNames', {anovaMeID, anovaSoAID, anovaCmpID});
                                
                clear  me soa cmp;
                                
            end % for shard samples            
            
        end % for analysis
        
    end % for measure
                   
    fprintf('\n\n######## Total elapsed time for comparing md1 and md2 wrt md3 ANOVA analyses on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

