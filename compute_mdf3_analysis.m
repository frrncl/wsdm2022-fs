%% compute_mdf3_analysis
% 
% Computes the Fake ANOVA on the shards.
% sharded corpus.

%% Synopsis
%
%   [] = compute_mdf3_analysis(trackID, shardID, tsID, startMeasure, endMeasure, startSample, endSample, threads)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|shardID|* - the identifier of the shards to be used.
% * *|tsID|* - the identifier of the topic sampling process to apply.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
% * *|startSample|* - the index of the start shard sample to process. Optional.
% * *|endSample|* - the index of the end shard sample to process. Optional.
% * *|threads|* - the maximum number of threads to be used. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2019-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = compute_mdf3_analysis(trackID, shardID, tsID, startMeasure, endMeasure, startSample, endSample, threads)

    persistent TAG MDFH0 MDFH1;
    
    if isempty(TAG)
        MDFH0 = 'mdf3h0';
        MDFH1 = 'mdf3h1';
        TAG = {MDFH1};      
    end

    % check the number of input parameters
    narginchk(3, 8);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
                 

    % check that shardID is a non-empty cell array
    validateattributes(shardID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');
    
    if iscell(shardID)
        % check that shardID is a cell array of strings with one element
        assert(iscellstr(shardID) && numel(shardID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    shardID = char(strtrim(shardID));
    shardID = shardID(:).';
    
    % check that shardID assumes a valid value
    validatestring(shardID, ...
        EXPERIMENT.shard.list, '', 'shardID');
    
    % check that the track and the shard rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.shard.(shardID).corpus), 'Track %s and shard %s do not rely on the same corpus', trackID, shardID);
      
    
    % check that tsID is a non-empty string
    validateattributes(tsID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'smplID');

    if iscell(tsID)
        % check that tsID is a cell array of strings with one element
        assert(iscellstr(tsID) && numel(tsID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected tsID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    tsID = char(strtrim(tsID));
    tsID = tsID(:).';
    
    % check that smplID assumes a valid value
    validatestring(tsID, ...
        EXPERIMENT.analysis.topicSampling.list, '', 'tsID');  
    
                     
    if nargin >= 4
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
        
    if nargin >= 6
        validateattributes(startSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.shard.sample }, '', 'startSample');
        
        validateattributes(endSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startSample, '<=', EXPERIMENT.shard.sample }, '', 'endSample');
    else 
        startSample = 1;
        endSample = EXPERIMENT.shard.sample;
    end
    
    if nargin == 8
        % the number of threads must be at maximum equal to the number of
        % physical cores
        validateattributes(threads, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', feature('numcores') }, '', 'threads');
        
        maxNumCompThreads(threads);
    else
        threads = maxNumCompThreads('automatic');
    end
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Performing Fake ANOVA analyses on shards on track %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * topic sampling: %s\n', tsID);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - shard: %s\n', shardID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.shard.(shardID).shard);
    fprintf('    * start sample: %d\n', startSample);
    fprintf('    * end sample: %d\n', endSample);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - threads: %d\n\n', threads);
    
    
    fprintf('\n+ Loading topic samples \n');
    
    tpc1ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(1, tsID, trackID);
    tpc2ID = EXPERIMENT.pattern.identifier.topicSampling.tpcID(2, tsID, trackID);
    
    serload2(EXPERIMENT.pattern.file.dataset.topic(trackID, EXPERIMENT.pattern.identifier.topicSampling.tsetID(tsID, trackID)), ...
        'WorkspaceVarNames', {'tpc1', 'tpc2'}, ...
        'FileVarNames', {tpc1ID, tpc2ID});
    
    % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading corpus data\n');
        
        mid = EXPERIMENT.measure.list{m};
        
        % load the whole corpus measure
        measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.track.(trackID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % the mean of the measure on the whole corpus
        mm = mean(measure{:, :});
        
        % order systems by descending mean of the measure on the whole corpus
        [~, idx] = sort(mm, 'descend');
        
        clear mm measure
        
        % for each analysis type
        for md = 1:length(TAG)
            
            start = tic;
            fprintf('  - performing %s ANOVA analysis\n', TAG{md});
            
            % the ANOVA model applied to each topic set
            MD.tpc1 = TAG{md};
            MD.tpc2 = TAG{md};
            
            
            
            % repeat the computation for each sample of shards
            for smpl = startSample:endSample
                
                fprintf('  - shard sample %d\n', smpl);
                
                fprintf('    * loading shard data\n');
                
                measures = cell(1, EXPERIMENT.shard.(shardID).shard);
                
                % for each shard, load the shard measures
                for shr = 1:EXPERIMENT.shard.(shardID).shard
                    
                    shrID = EXPERIMENT.pattern.identifier.shard.full(shardID, shr, smpl);
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(mid, shrID, trackID);
                    
                    serload2(EXPERIMENT.pattern.file.measure.shard(trackID, shardID, measureID), ...
                        'WorkspaceVarNames', {'measure'}, ...
                        'FileVarNames', {measureID});
                    
                    tmp = measure{:, :};
                    
                    % always use zero balancing since it does not matter for
                    % md3
                    tmp(isnan(tmp)) = 0;
                    
                    measure{:, :} = tmp;
                    
                    % order runs as on the whole corpus
                    measure = measure(:, idx);
                    
                    measures{shr} = measure;
                    
                    clear tmp measure;
                    
                end % for shard
                
                
                % the number of topics, runs, and shards
                T.tpc1 = size(tpc1, 2);
                T.tpc2 = size(tpc2, 2);
                T.size = height(measures{1});
                R = width(measures{1});
                S = EXPERIMENT.shard.(shardID).shard;
                
                % total number of elements in the list
                N.tpc1 = T.tpc1 * R * S;
                N.tpc2 = T.tpc2 * R * S;
                
                % preallocate data structures
                [tbl, soa, me, cmp] = md_data_structures(measures{1}.Properties.VariableNames, R, T, MD);
                
                % perform analyses on each topic sample
                for ts = 1:EXPERIMENT.analysis.topicSampling.samples
                    
                    fprintf('    * topic set sample %d\n', ts);
                    
                    start = tic;
                    
                    fprintf('      # fitting the ANOVA model\n');
                    
                    % layout the data for the ANOVA
                    [data, subject, factorA, factorB] = layout_anova_data(measures, tpc1(ts, :), tpc2(ts, :), T, R, S, N);
                    
                    % perform the ANOVA analysis
                    [~, tbl.tpc1{ts}, sts.tpc1] = EXPERIMENT.analysis.(TAG{md}).compute(data.tpc1, subject.tpc1, factorA.tpc1, factorB.tpc1);
                    [~, tbl.tpc2{ts}, sts.tpc2] = EXPERIMENT.analysis.(TAG{md}).compute(data.tpc2, subject.tpc2, factorA.tpc2, factorB.tpc2);
                    
                    % extract info from the ANOVA tables
                    sts.df_factorA.tpc1 = tbl.tpc1{ts}{3,3};
                    sts.ss_factorA.tpc1 = tbl.tpc1{ts}{3,2};
                    sts.F_factorA.tpc1 = tbl.tpc1{ts}{3,6};
                    
                    sts.df_error.tpc1 = tbl.tpc1{ts}{8,3};
                    sts.ss_error.tpc1 = tbl.tpc1{ts}{8,2};
                    sts.ms_error.tpc1 = tbl.tpc1{ts}{8,5};
                    
                    sts.df_factorA.tpc2 = tbl.tpc2{ts}{3,3};
                    sts.ss_factorA.tpc2 = tbl.tpc2{ts}{3,2};
                    sts.F_factorA.tpc2 = tbl.tpc2{ts}{3,6};
                    
                    sts.df_error.tpc2 = tbl.tpc2{ts}{8,3};
                    sts.ss_error.tpc2 = tbl.tpc2{ts}{8,2};
                    sts.ms_error.tpc2 = tbl.tpc2{ts}{8,5};

                    % indicate that power has not to be computed
                    sts.computePower = false;
                    
                    % compute SoA, power and main effects (Tukey, multiple
                    % comparisons, ...)
                    [soa, me] = md_factorA_analysis(TAG{md}, ts, sts, N, T, R, S, data, factorA, soa, me);
                    
                    % tweak the computations
                soa.omega2p.factorA.tpc1(ts) = NaN;
                soa.omega2p.factorA.tpc2(ts) = NaN;
                
                me.factorA.tukey.q.tpc1(ts) = NaN;
                me.factorA.tukey.q.tpc2(ts) = NaN;
                
                me.factorA.tukey.halfWidth.tpc1(ts) = NaN;
                me.factorA.tukey.halfWidth.tpc2(ts) = NaN;
                                
                switch TAG{md}
                    case MDFH0 % set everything to not significant
                        me.factorA.tukey.mc.tpc1{ts}(:, 6) = 1.0;
                        me.factorA.tukey.mc.tpc2{ts}(:, 6) = 1.0;
                    case MDFH1 % set everything to significant
                        me.factorA.tukey.mc.tpc1{ts}(:, 6) = 0.0;
                        me.factorA.tukey.mc.tpc2{ts}(:, 6) = 0.0;
                end
                    
                    % compare the two topic sets on the above analyses
                    [cmp] = md_factorA_compare_ts(ts, me, soa, cmp);
                    
                    fprintf('      # elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                    
                end  % for topic samples
                
                
                % compute summary statistics about the comparison between the
                % two topic sets;
                [cmp] = md_factorA_summary_ts(cmp);
                
                fprintf('    * saving the analyses\n');
                
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG{md}, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG{md}, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG{md}, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG{md}, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                anovaCmpID = EXPERIMENT.pattern.identifier.anova.cmp(TAG{md}, mid, EXPERIMENT.pattern.identifier.shard.short(shardID, smpl), tsID, trackID);
                
                sersave2(EXPERIMENT.pattern.file.analysis.shard(trackID, shardID, anovaID), ...
                    'WorkspaceVarNames', {'me', 'tbl', 'soa', 'cmp'}, ...
                    'FileVarNames', {anovaMeID, anovaTableID, anovaSoAID, anovaCmpID});
                
                clear measures T R  N data me tbl sts soa cmp;
                
            end % for each shard sample
            
        end % for analysis
        
        clear idx
        
    end % for measure
    
    fprintf('\n\n######## Total elapsed time for performing Fake ANOVA analyses on shards on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

