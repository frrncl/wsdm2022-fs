%% split_pool
% 
% Splits a pool into a set of shards.
%
%% Synopsis
%
%   [] = split_pool(trackID, shardID, startSample, endSample)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|shardID|* - the identifier of the shards to be used.
% * *|startSample|* - the index of the start shard sample to process. Optional.
% * *|endSample|* - the index of the end shard sample to process. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = split_pool(trackID, shardID, startSample, endSample)
    
    % check that we have the correct number of input arguments. 
    narginchk(2, 4);
    
    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');    
    
    % check that shardID is a non-empty string
    validateattributes(shardID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'shardID');

    if iscell(shardID)
        % check that shardID is a cell array of strings with one element
        assert(iscellstr(shardID) && numel(shardID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected shardID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    shardID = char(strtrim(shardID));
    shardID = shardID(:).';
    
    % check that shardID assumes a valid value
    validatestring(shardID, ...
        EXPERIMENT.shard.list, '', 'shardID');  
    
    % check that the track and the shard rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.shard.(shardID).corpus), 'Track %s and shard %s do not rely on the same corpus', trackID, shardID);
    
    if nargin == 4
        validateattributes(startSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.shard.sample }, '', 'startSample');
        
        validateattributes(endSample, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startSample, '<=', EXPERIMENT.shard.sample }, '', 'endSample');
    else 
        startSample = 1;
        endSample = EXPERIMENT.shard.sample;
    end
            
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Splitting pool for track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - shard: %s\n', shardID);
    fprintf('    + corpus: %s\n', EXPERIMENT.shard.(shardID).corpus);
    fprintf('    + number of shards: %d\n', EXPERIMENT.shard.(shardID).shard);
    fprintf('    + start sample: %d\n', startSample);
    fprintf('    + end sample: %d\n', endSample);    


    fprintf('+ Loading pool of the corpus\n');
        
    corpusPoolID = EXPERIMENT.pattern.identifier.pool(EXPERIMENT.shard.(shardID).corpus, trackID);
    fprintf('  - %s\n', corpusPoolID);
    
    serload2(EXPERIMENT.pattern.file.dataset.corpus(trackID, corpusPoolID), ...
        'WorkspaceVarNames', {'corpusPool'}, ...
        'FileVarNames', {corpusPoolID});
    
    % the total number of topics in the pool
    T = height(corpusPool);
    

    % create a dummy table with a dummy document and the lowest relevance
    % degree possible among those in the pool    
    dummyTable = corpusPool{:, 1}{1, 1}(:, :);
    
    relevanceDegrees = categories(dummyTable.RelevanceDegree);

    idx = find(dummyTable.RelevanceDegree == relevanceDegrees(1), 1, 'first');
    
    dummyTable = dummyTable(idx, :);
    dummyTable(1, 'Document') = {'#####DUMMY_DOC_ID^^^^^^'};
    
    
    
    fprintf('+ Processing shards\n');
    
    % repeat the splitting for each sample
    for smpl = startSample:endSample
        
        fprintf('  - sample %d\n', smpl);
        
        % for each shard
        for shr = 1:EXPERIMENT.shard.(shardID).shard
            
            start = tic;
            
            shrID = EXPERIMENT.pattern.identifier.shard.full(shardID, shr, smpl);
            
            fprintf('    * shard %s... ', shrID);
            
            shard = importdata(EXPERIMENT.pattern.file.shard(shardID, shrID));
            
            shardPool = corpusPool;
            
            % for each topic
            for t = 1:T
                
                % extract the inner table contained in the topic
                topic = shardPool{t, 1}{1, 1};
                
                % find which rows (documents) have to be kept
                keep = ismember(topic.Document, shard);
                
                if any(keep)
                    shardPool{t, 1} = {topic(keep, :)};
                else
                    shardPool{t, 1} = {dummyTable};
                end
                
            end % for topic
                        
            shardPoolID = EXPERIMENT.pattern.identifier.pool(shrID, trackID);
            shardPool.Properties.UserData.identifier = shardPoolID;
            shardPool.Properties.VariableNames = {shardPoolID};
            
            sersave2(EXPERIMENT.pattern.file.dataset.shard(trackID, shardID, shardPoolID), ...
                'WorkspaceVarNames', {'shardPool'}, ...
                'FileVarNames', {shardPoolID});
            
            clear shard shardPool
            
            fprintf('elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            
        end % for shard
        
    end % for sample
    
    fprintf('\n\n######## Total elapsed time for splitting pool for track %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
end

